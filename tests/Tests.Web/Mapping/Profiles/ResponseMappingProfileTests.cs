﻿using Application.Common;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Tests.Common.Builders;
using Tests.Common.Mapping;
using Tests.Web.TestCollections;
using Web.Constants;
using Web.Features.Common;
using Web.Mapping.Profiles;

namespace Tests.Web.Mapping.Profiles;

[Collection(WebTestCollection.NAME)]
public class ResponseMappingProfileTests
{
    private const string ANY_ERROR_DESCRIPTION = "Could not create user.";

    private const string NAME_FR = "Guide de la réglementation en copropriété";
    private const string NAME_EN = "Guide to condominium regulations";
    private const string DESCRIPTION_FR = "On vise, en copropriété, à assurer aux occupant un milieu de vie paisible.";
    private const string DESCRIPTION_EN = "We aim, in co-ownership, to provide occupants with a peaceful living environment.";
    private const string ISBN = "978-2-89689-559-5";
    private const string AUTHOR = "Christine Gagnon, Yves Papineau";
    private const string EDITOR = "Wilson & Lafleur";
    private const string CARD_IMAGE = "www.google.com";
    private const int YEAR_OF_PUBLICATION = 2023;
    private const int NUMBER_OF_PAGES = 346;

    private readonly BookBuilder _bookBuilder;

    private readonly IMapper _mapper;

    public ResponseMappingProfileTests()
    {
        _bookBuilder = new BookBuilder();

        _mapper = new MapperBuilder().WithProfile<ResponseMappingProfile>().Build();
    }

    [Fact]
    public void GivenSuccessfulIdentityResult_WhenMap_ThenSucceededIsTrue()
    {
        // Arrange
        var identityResult = IdentityResult.Success;

        // Act
        var succeededOrNotResponse = _mapper.Map<SucceededOrNotResponse>(identityResult);

        // Assert
        succeededOrNotResponse.Succeeded.ShouldBeTrue();
    }

    [Fact]
    public void GivenFailedIdentityResult_WhenMap_ThenSucceededContainsErrorWithIdentityCodeAsErrorType()
    {
        // Arrange
        var error = new IdentityError { Code = IdentityResultExceptions.USER_ALREADY_HAS_PASSWORD };
        var identityResult = IdentityResult.Failed(error);

        // Act
        var succeededOrNotResponse = _mapper.Map<SucceededOrNotResponse>(identityResult);

        // Assert
        succeededOrNotResponse.Errors.ShouldContain(x => x.ErrorType == error.Code);
    }

    [Fact]
    public void GivenFailedIdentityResult_WhenMap_ThenSucceededContainsErrorWithIdentityErrorDescriptionAsErrorMessage()
    {
        // Arrange
        var error = new IdentityError { Description = ANY_ERROR_DESCRIPTION };
        var identityResult = IdentityResult.Failed(error);

        // Act
        var succeededOrNotResponse = _mapper.Map<SucceededOrNotResponse>(identityResult);

        // Assert
        succeededOrNotResponse.Errors.ShouldContain(x => x.ErrorMessage == ANY_ERROR_DESCRIPTION);
    }

    [Fact]
    public void GivenIdentityErrorWithCode_WhenMap_ThenErrorHasIdentityResultCodeAsErrorType()
    {
        // Arrange
        var identityError = new IdentityError { Code = IdentityResultExceptions.USER_ALREADY_HAS_PASSWORD };

        // Act
        var error = _mapper.Map<Error>(identityError);

        // Assert
        error.ErrorType.ShouldBe(IdentityResultExceptions.USER_ALREADY_HAS_PASSWORD);
    }

    [Fact]
    public void GivenIdentityErrorWithCode_WhenMap_ThenErrorHasIdentityResultDescriptionAsErrorMessage()
    {
        // Arrange
        var identityError = new IdentityError { Description = ANY_ERROR_DESCRIPTION };

        // Act
        var error = _mapper.Map<Error>(identityError);

        // Assert
        error.ErrorMessage.ShouldBe(ANY_ERROR_DESCRIPTION);
    }

}