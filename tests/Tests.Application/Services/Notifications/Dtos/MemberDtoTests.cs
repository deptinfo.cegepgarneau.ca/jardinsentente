using Application.Services.Notifications.Dtos;

namespace Tests.Application.Services.Notifications.Dtos;

public class MemberDtoTests
{
    private const string ANY_FIRST_NAME = "john";
    private const string ANY_LAST_NAME = "doe";
    private const string ANY_EMAIL = "john.doe@gmail.com";
    private const string ANY_PHONE_NUMBER = "514-640-8920";
    private const int ANY_APARTMENT = 2;
    private const string ANY_STREET = "965, Hollywood Blvd";
    private const string ANY_CITY = "Hollywood";
    private const string ANY_ZIP_CODE = "G7E 3L8";
    
    [Fact]
    public void GivenAnyFirstName_WhenNewMemberDto_ThenFirstNameShouldBeSameAsGivenFirstName()
    {
        // Act
        var memberDto = new PersonDto { FirstName = ANY_FIRST_NAME };

        // Assert
        memberDto.FirstName.ShouldBe(ANY_FIRST_NAME);
    }
    
    [Fact]
    public void GivenAnyLastName_WhenNewMemberDto_ThenLastNameShouldBeSameAsGivenLastName()
    {
        // Act
        var memberDto = new PersonDto { LastName = ANY_LAST_NAME };

        // Assert
        memberDto.LastName.ShouldBe(ANY_LAST_NAME);
    }
    
    [Fact]
    public void GivenAnyEmail_WhenNewMemberDto_ThenEmailAddressShouldBeSameAsGivenEmailAddress()
    {
        // Act
        var memberDto = new PersonDto { Email = ANY_EMAIL };

        // Assert
        memberDto.Email.ShouldBe(ANY_EMAIL);
    }
    
    [Fact]
    public void GivenAnyPhoneNumber_WhenNewMemberDto_ThenPhoneNumberShouldBeSameAsGivenPhoneNumber()
    {
        // Act
        var memberDto = new PersonDto { PhoneNumber = ANY_PHONE_NUMBER };

        // Assert
        memberDto.PhoneNumber.ShouldBe(ANY_PHONE_NUMBER);
    }
    
    [Fact]
    public void GivenAnyApartment_WhenNewMemberDto_ThenApartmentShouldBeSameAsGivenApartment()
    {
        // Act
        var memberDto = new PersonDto { Apartment = ANY_APARTMENT };

        // Assert
        memberDto.Apartment.ShouldBe(ANY_APARTMENT);
    }
    
    [Fact]
    public void GivenAnyStreet_WhenNewMemberDto_ThenStreetShouldBeSameAsGivenStreet()
    {
        // Act
        var memberDto = new PersonDto { Street = ANY_STREET };

        // Assert
        memberDto.Street.ShouldBe(ANY_STREET);
    }
    
    [Fact]
    public void GivenAnyCity_WhenNewMemberDto_ThenCityShouldBeSameAsGivenCity()
    {
        // Act
        var memberDto = new PersonDto { City = ANY_CITY };

        // Assert
        memberDto.City.ShouldBe(ANY_CITY);
    }
    
    [Fact]
    public void GivenAnyZipCode_WhenNewMemberDto_ThenZipCodeShouldBeSameAsGivenZipCode()
    {
        // Act
        var memberDto = new PersonDto { ZipCode = ANY_ZIP_CODE };

        // Assert
        memberDto.ZipCode.ShouldBe(ANY_ZIP_CODE);
    }
}