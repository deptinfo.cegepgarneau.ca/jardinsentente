﻿using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Repositories;
using Infrastructure.Repositories.Members;
using Microsoft.AspNetCore.Identity;
using Tests.Common.Fixtures;
using Tests.Infrastructure.TestCollections;
namespace Tests.Infrastructure.Repositories.Members;

[Collection(InfrastructureTestCollection.NAME)]
public class MemberRepositoryTests
{
    private readonly TestFixture _testFixture;

    private readonly IMemberRepository _memberRepository;
    private readonly UserManager<User> _userManager;

    public MemberRepositoryTests(UserManager<User> userManager, TestFixture testFixture)
    {
        _testFixture = testFixture;
        _userManager = userManager;
       // _memberRepository = new MemberRepository(_userManager, _testFixture.DbContext);
    }
    /*
    [Fact]
    public async Task WhenGetContactCount_ThenReturnNumberOfMembersInDatabase()
    {
        // Arrange
        await GivenDatabaseHasOrganizationMember();
        var expectedCount = _testFixture.DbContext.Members.Count();

        // Act
        var contactCount = _memberRepository.GetMemberCount();

        // Assert
        contactCount.ShouldBe(expectedCount);
    }
    
    [Fact]
    public async Task WhenFindByUserId_ThenReturnMemberAssociatedWithUser()
    {
        // Arrange
        var expectedMember = await GivenDatabaseHasOrganizationMember();

        // Act
        var actualMember = _memberRepository.FindByUserId(expectedMember.User.Id);

        // Assert
        actualMember?.Id.ShouldBe(expectedMember.Id);
    }
    
    [Fact]
    public async Task WhenFindByUserEmail_ThenReturnMemberAssociatedWithUser()
    {
        // Arrange
        var expectedMember = await GivenDatabaseHasOrganizationMember();

        // Act
        var actualMember = _memberRepository.FindByUserEmail(expectedMember.User.Email);

        // Assert
        actualMember?.Email.ShouldBe(expectedMember.Email);
    }

    private async Task<Member> GivenDatabaseHasOrganizationMember()
    {
        var user = await _testFixture.GivenUserInDatabase();
        var member = _testFixture.MemberBuilder.WithUser(user).Build();
        _testFixture.DbContext.Members.Add(member);
        await _testFixture.DbContext.SaveChangesAsync();
        return member;
    }*/
}