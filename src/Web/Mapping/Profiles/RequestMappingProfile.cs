﻿using AutoMapper;
using Domain.Common;
using Domain.Entities;
using Domain.Entities.Plan;
using Domain.Entities.Event;
using Domain.Entities.WaitingMembers;
using Web.Dtos;
using Web.Features.Plan.CreatePlan;
using Web.Features.Plan.EditPlan;
using Web.Features.Events.CreateEvent;
using Web.Features.Events.EditEvent;
using Web.Features.Regles.CreateRegle;
using Web.Features.Regles.EditRegle;
using Web.Features.WaitingMembers.CreateWaitingMember;
using Web.Features.Admins.Members.CreateMember;
using Web.Features.Notifications.CreateNotification;
using Domain.Entities.Notifications;
using Domain.Entities.Lots;

namespace Web.Mapping.Profiles;

public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {
        CreateMap<TranslatableStringDto, TranslatableString>().ReverseMap();

        CreateMap<CreateRegleRequest, Regle>();
        CreateMap<EditRegleRequest, Regle>();

        CreateMap<CreateWaitingMemberRequest, WaitingMember>();
        CreateMap<EditEventRequest, Event>();
        CreateMap<CreateEventRequest, Event>();

        CreateMap<CreateNotificationRequest, Notification>();

        CreateMap<CreatePlanRequest, Plan>();
        CreateMap<EditPlanRequest, Plan>()
            .ForMember(dest => dest.UserLotId, opt => opt.MapFrom(src => src.IdUserLot));

        CreateMap<CreateMemberRequest, Member>();
    }
}