﻿using Application.Common;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Books;
using Domain.Entities.WaitingMembers;
using Microsoft.AspNetCore.Identity;
using Web.Features.Common;
using Web.Features.Members.Me.GetMe;
using Web.Features.Regles;
using Web.Features.Admins.Members;
using Domain.ValueObjects;
using Web.Features.Invite.WaitingMembers;
using Domain.Entities.Lots;
using Web.Features.Lots;
using Web.Features.UserLot;
using Web.Features.Notifications;
using Domain.Entities.Plan;
using Web.Features.Plan;
using Web.Features.Events;
using Domain.Entities.Event;
using Domain.Entities.Notifications;

namespace Web.Mapping.Profiles;

public class ResponseMappingProfile : Profile
{
    public ResponseMappingProfile()
    {
        CreateMap<IdentityResult, SucceededOrNotResponse>();

        CreateMap<IdentityError, Error>()
            .ForMember(error => error.ErrorType, opt => opt.MapFrom(identity => identity.Code))
            .ForMember(error => error.ErrorMessage, opt => opt.MapFrom(identity => identity.Description));

        CreateMap<Member, GetMeResponse>()
            .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.User.RoleNames))
            .ForMember(x => x.PhoneNumber, opt => opt.MapFrom(x => x.GetPhoneNumber()))
            .ForMember(x => x.PhoneExtension, opt => opt.MapFrom(x => x.GetPhoneExtension()));

        CreateMap<Regle, RegleDto>();

        CreateMap<Member, MemberDto>()
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
            .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.User.RoleNames))
            .ForMember(dest=>dest.PhoneNumber,opt=>opt.MapFrom(src=>src.GetPhoneNumber()))
            .ForMember(dest => dest.Lots, opt => opt.MapFrom(src => src.User.UserLots.Select(l => l.Lot)))
            .AfterMap((member, dto, context) =>
            {
                dto.Lots = member.User.UserLots.Select(lot => context.Mapper.Map<LotDto>(lot.Lot)).ToList();
            });


        CreateMap<Lot, LotDto>();

        CreateMap<WaitingMember, WaitingMemberDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToDateTimeUtc()))
            .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
            .ForMember(dest => dest.Adress, opt => opt.MapFrom(src => src.Adress))
            .ForMember(dest => dest.AppNumber, opt => opt.MapFrom(src => src.AppNumber))
            .ForMember(dest => dest.PostalCode, opt => opt.MapFrom(src => src.PostalCode))
            .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email));

        CreateMap<Event, EventDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Month, opt => opt.MapFrom(src => src.Month))
            .ForMember(dest => dest.Jour, opt => opt.MapFrom(src => src.Jour))
            .ForMember(dest => dest.DayOfWeek, opt => opt.MapFrom(src => src.DayOfWeek))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description));


        CreateMap<Notification, NotificationDto>();


        CreateMap<UserLot, UserLotDto>()
            .ForMember(userLotDto=>userLotDto.LotNumber, opt=>opt.MapFrom(userLot=>userLot.Lot.LotNumber));

        CreateMap<Plan, PlanDto>();
        

    }

}