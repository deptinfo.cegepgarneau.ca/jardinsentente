﻿using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.Inscription;


public class InscriptionViewModel
{
    [Required(ErrorMessage = "Veuillez entrer votre prénom")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "Votre prénom doit contenir entre 3 et 50 charactères.")]
    public string FirstName { get; set; } = default!;

    [Required(ErrorMessage = "Veuillez entrer votre nom")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "Votre nom doit contenir entre 3 et 50 charactères.")]
    public string LastName { get; set; } = default!;

    [Required(ErrorMessage = "Veuillez entrer votre adresse")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "Votre adresse doit contenir entre 3 et 255 charactères.")]
    public string Adress { get; set; } = default!;

    [RegularExpression(@"^\d+$", ErrorMessage = "Le numéro d'appartement doit être un chiffre.")]
    public int? AppNb { get; set; }

    [Required(ErrorMessage = "Veuillez entrer votre code postal")]
    [RegularExpression(@"^[A-Z]\d[A-Z] *\d[A-Z]\d$", ErrorMessage = "Le code postal doit être au format valide (A1A1A1).")]
    public string PostalCode { get; set; } = default!;

    [Required(ErrorMessage = "Veuillez entrer votre numéro de téléphone")]
    [RegularExpression(@"^\d{10}$", ErrorMessage = "Le numéro de téléphone doit être au format valide (4181234567).")]
    public string PhoneNumber { get; set; } = default!;

    [Required(ErrorMessage = "Veuillez entrer votre adresse courriel")]
    [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Veuillez entrer une adresse courriel valide")]
    public string Email { get; set; } = default!;

    [Required(ErrorMessage = "Veuillez choisir de quelle institution vous venez")]
    public string From { get; set; } = default!;
}