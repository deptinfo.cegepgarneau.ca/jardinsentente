using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Web.Extensions;

namespace Web.Controllers;

[Route("accueil")]
public class HomePageController : BaseController 
{
    private readonly IStringLocalizer<HomePageController> _localizer;
    public HomePageController(ILogger<HomeController> logger, IStringLocalizer<HomePageController> localizer) : base(logger)
    {
        _localizer = localizer;
    }

    public IActionResult Index()
    {
        ViewBag.IsHomePage = true;
        return View();
    }

}