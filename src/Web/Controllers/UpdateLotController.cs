﻿using Application.Interfaces.Services.Lots;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Web.Controllers
{
    public class UpdateLotController : BaseController
    {
        private readonly ILotUpdateService _lotUpdateService;

        public class donnees
        {
            public int LotNumber { get; set; }
            public double Temperature { get; set; }
            public double Humidity { get; set; }
        }

        public UpdateLotController(ILogger<UpdateLotController> logger, ILotUpdateService lotUpdateService) : base(logger)
        {
            _lotUpdateService = lotUpdateService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index([FromBody] donnees jsonString)
        {
            try
            {
                if (jsonString != null && jsonString.LotNumber != 0)
                {
                    await _lotUpdateService.UpdateLot(jsonString.LotNumber, jsonString.Temperature, jsonString.Humidity);
                    return Ok($"Mis à jour réussi du lot {jsonString.LotNumber} avec les données "+jsonString);
                }
                throw new Exception();
            }
            catch (Exception)
            {

                return BadRequest("Erreur lors de la mise a jour avec le esp32");
            }
        }
    }
}
