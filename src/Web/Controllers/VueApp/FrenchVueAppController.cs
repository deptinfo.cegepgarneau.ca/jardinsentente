﻿using Application.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Cookies;
using Web.Extensions;

namespace Web.Controllers.VueAPp;

[Authorize]
[Route("/fr")]
public class FrenchVueAppController : BaseController
{
    public FrenchVueAppController(ILogger<FrenchVueAppController> logger) : base(logger)
    {
    }

    [HttpGet]
    public IActionResult Index()
    {
        ChangeCultureToFrench();
        ViewData["CurrentUrl"] = null;

        return View("VueAppPage");
    }

    [HttpGet]
    [Route("{**catchall}")]
    public IActionResult RedirectToSamePath()
    {
        ChangeCultureToFrench();
        ViewData["CurrentUrl"] = Request.Path.Value;

        return View("VueAppPage");
    }

    private void ChangeCultureToFrench()
    {
        CultureHelper.ChangeCurrentCultureTo("fr");
        HttpContext.SetAspNetLanguageCookie("fr");
        HttpContext.SetCookieValue(CookieNames.LANGUAGE_COOKIE_NAME, "fr", false, false);
    }
}