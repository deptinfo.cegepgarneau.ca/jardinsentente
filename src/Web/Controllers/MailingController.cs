﻿using Application.Services.Notifications;
using Application.Services.Notifications.Dtos;
using Application.Services.Notifications.Models;
using Infrastructure.Mailing;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MailingController : BaseController
    {

        private readonly EmailNotificationService _emailNotificationService;

        public MailingController(EmailNotificationService emailNotificationService, ILogger<MailingController> logger) : base(logger)
        {
            _emailNotificationService = emailNotificationService;
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendMail([FromBody] GroupNotificationModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recipients = model.Recipients;
            var subject = model.Subject;
            var message = model.Message;

            var response = await _emailNotificationService.SendGroupNotification(recipients, subject, message);

            if (response.Successful)
            {
                return Ok("Mail sent");
            } else
            {
                var errorDto = new SendNotificationResponseDto(false, response?.ErrorMessages);
                return BadRequest(errorDto);
            }
        }
    }
}
