﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ErrorController(ILogger<BaseController> logger) : base(logger)
        {
        }

        [Route("Error/404")]
        public IActionResult Error404()
        {
            return View("Error404");
        }
    }
}
