﻿using Core.Flash;
using Domain.Entities.WaitingMembers;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Extensions;
using Web.ViewModels.Inscription;

namespace Web.Controllers;

[Route("inscription")]
public class InscriptionController : BaseController
{
    private readonly IFlasher _flasher;
    private readonly IWaitingMemberRepository _waitingMemberRepository;
    public InscriptionController(ILogger<InscriptionController> logger, IWaitingMemberRepository waitingMemberRepository, IFlasher flasher) : base(logger)
    {
        _waitingMemberRepository = waitingMemberRepository;
        _flasher = flasher;
    }

    public IActionResult Index()
    {
        return View();
    }

    [Microsoft.AspNetCore.Mvc.HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Index(InscriptionViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }
        bool emailAlreadyUsed = _waitingMemberRepository.FindByEmail(model.Email);
        if (emailAlreadyUsed)
        {
            _flasher.Flash(Types.Warning, "L'adresse courriel est déjà utilisé", true);
            return View(model);
        }
        var waitingMember = new WaitingMember();
        waitingMember.SetFirstName(model.FirstName);
        waitingMember.SetLastName(model.LastName);
        waitingMember.SetAdress(model.Adress);
        waitingMember.SetAppNumber(model.AppNb);
        waitingMember.SetPostalCode(model.PostalCode);
        waitingMember.SetEmail(model.Email);
        waitingMember.SetFrom(model.From);
        waitingMember.SetPhoneNumber(model.PhoneNumber);
        await _waitingMemberRepository.CreateWaitingMember(waitingMember);
        _flasher.Flash(Types.Success, "Inscription effectuée avec succès", true);
        return View();
    }
}