﻿using Application.Interfaces.Services.Plan;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Plan.CreatePlan
{
    public class CreatePlanEndPoint : Endpoint<CreatePlanRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IPlanCreationService _planRepository;


        public CreatePlanEndPoint(IMapper mapper, IPlanCreationService planRepository)
        {
            _mapper = mapper;
            _planRepository = planRepository;
        }


        public override void Configure()
        {
            DontCatchExceptions();

            Post("plan");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreatePlanRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Domain.Entities.Plan.Plan>(req);

            await _planRepository.CreatePlan(request,req.IdUserLot);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
