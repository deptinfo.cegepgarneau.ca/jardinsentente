﻿namespace Web.Features.Plan.CreatePlan
{
    public class CreatePlanRequest
    {
        public string Nom { get; set; } = default!;
        public double X { get; set; } = default!;
        public double Y { get; set; } = default!;
        public double Largeur { get; set; } = default!;
        public double Longueur { get; set; } = default!;
        public Guid IdUserLot { get; set; } = default!;
    }
}
