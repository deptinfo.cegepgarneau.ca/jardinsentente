﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Regles.DeleteRegle;

namespace Web.Features.Plan.DeletePlan
{
    public class DeletePlanEndPoint : Endpoint<DeletePlanRequest, EmptyResponse>
    {
        private readonly IPlanRepository _planRepository;

        public DeletePlanEndPoint(IPlanRepository planRepository)
        {
            _planRepository = planRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("plan/{id}");
            //Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeletePlanRequest request, CancellationToken ct)
        {
            await _planRepository.DeletePlanWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
