﻿namespace Web.Features.Plan.DeletePlan
{
    public class DeletePlanRequest
    {
        public Guid Id { get; set; }
    }
}
