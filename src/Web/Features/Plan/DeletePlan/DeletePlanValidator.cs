﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Plan.DeletePlan
{
    public class DeletePlanValidator:Validator<DeletePlanRequest>
    {
        public DeletePlanValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Regle id is required.");
        }
    }
}
