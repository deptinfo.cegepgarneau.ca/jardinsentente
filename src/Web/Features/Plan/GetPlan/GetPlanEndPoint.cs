﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Plan.GetAllPlanWithidUserLot;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Plan.GetPlan
{
    public class GetPlanEndPoint : Endpoint<GetPlanRequest, PlanDto>
    {
        private readonly IMapper _mapper;
        private readonly IPlanRepository _planRepository;

        public GetPlanEndPoint(IMapper mapper, IPlanRepository planRepository)
        {
            _mapper = mapper;
            _planRepository = planRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("plan/{id}");
            // Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetPlanRequest request, CancellationToken ct)
        {
            var plan = _planRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<PlanDto>(plan), ct);
        }
    }
}
