﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Plan.GetPlan
{
    public class GetPlanValidator:Validator<GetPlanRequest>
    {
        public GetPlanValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyPlanId")
                .WithMessage("L'id du plan ne doit pas être nulle");
        }
    }
}
