﻿namespace Web.Features.Plan
{
    public class PlanDto
    {
        public Guid Id { get; set; }
        public string Nom { get;  set; } = default!;
        public double X { get;  set; } = default!;
        public double Y { get;  set; } = default!;
        public double Largeur { get;  set; } = default!;
        public double Longueur { get;  set; } = default!;
    }
}
