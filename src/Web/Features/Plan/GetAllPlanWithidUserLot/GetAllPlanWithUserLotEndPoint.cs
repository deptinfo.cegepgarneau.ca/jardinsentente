﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Plan.GetAllPlanWithidUserLot
{
    public class GetAllPlanWithUserLotEndPoint: Endpoint<GetAllPlanWithUserLotRequest, List<PlanDto>>
    {
        private readonly IMapper _mapper;
        private readonly IPlanRepository _planRepository;

        public GetAllPlanWithUserLotEndPoint(IMapper mapper, IPlanRepository planRepository)
        {
            _mapper = mapper;
            _planRepository = planRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("plan/userlot/{id}");
            // Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetAllPlanWithUserLotRequest request, CancellationToken ct)
        {
            var plan = _planRepository.GetAll(request.Id);
            
            await SendOkAsync(_mapper.Map<List<PlanDto>>(plan), ct);
        }
    }
}
