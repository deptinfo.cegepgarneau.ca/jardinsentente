﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Plan.GetAllPlanWithidUserLot
{
    public class GetAllPlanWithUserLotValidator: Validator<GetAllPlanWithUserLotRequest>
    {
        public GetAllPlanWithUserLotValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyPlanId")
                .WithMessage("L'id du plan ne doit pas être nulle");
        }
    }
}
