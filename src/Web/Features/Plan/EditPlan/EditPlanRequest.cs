﻿namespace Web.Features.Plan.EditPlan
{
    public class EditPlanRequest
    {
        public string Id { get; set; } = default!;
        public string Nom { get; set; } = default!;
        public double X { get; set; } = default!;
        public double Y { get; set; } = default!;
        public double Largeur { get; set; } = default!;
        public double Longueur { get; set; } = default!;
        public Guid IdUserLot { get; set; } = default!;
    }
}
