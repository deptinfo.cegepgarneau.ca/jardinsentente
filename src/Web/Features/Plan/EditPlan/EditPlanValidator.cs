﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Plan.EditPlan
{
    public class EditPlanValidator : Validator<EditPlanRequest>
    {
        const double MIN = 0;
        const double LONGUEUR = 12;
        const double LARGEUR = 6.4;
        public EditPlanValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("ID vide")
                .WithMessage("L'ID ne peut pas être nulle ou vide");

            RuleFor(x => x.Nom)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidNom")
                .WithMessage("Le nom du plan ne peut pas être nulle ou vide");

            RuleFor(x => x.X)
                .NotNull()
                .GreaterThanOrEqualTo(MIN)
                .LessThanOrEqualTo(LONGUEUR)
                .WithErrorCode("InvalidX")
                .WithMessage($"x doit être compris entre {MIN} et {LONGUEUR}");

            RuleFor(x => x.Y)
                .NotNull()
                .LessThanOrEqualTo(LARGEUR)
                .GreaterThanOrEqualTo(MIN)
                .WithErrorCode("InvalidY")
                .WithMessage($"y doit être compris entre {MIN} et {LARGEUR}");


            RuleFor(x => x.Largeur)
                .NotNull()
                .GreaterThanOrEqualTo(MIN)
                .LessThanOrEqualTo(LARGEUR)
                .WithErrorCode("InvalidLargeur")
                .WithMessage($"La largeur doit être compris entre {MIN} et {LARGEUR}");


            RuleFor(x => x.Longueur)
                .NotNull()
                .GreaterThanOrEqualTo(MIN)
                .LessThanOrEqualTo (LONGUEUR)
                .WithErrorCode("InvalidLongueur")
                .WithMessage($"La longueur doit être comrpis entre {MIN} et {LONGUEUR}");

            RuleFor(x => x.IdUserLot)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("ID vide")
                .WithMessage("L'ID ne peut pas être nulle ou vide");

            RuleFor(x => x.X + x.Longueur)
                .NotNull()
                .GreaterThanOrEqualTo(MIN)
                .LessThanOrEqualTo(LONGUEUR)
                .WithErrorCode("InvalidXLongueur")
                .WithMessage($"La longueur et X doit être comrpis entre {MIN} et {LONGUEUR}");

            RuleFor(x => x.Y + x.Largeur)
                .NotNull()
                .GreaterThanOrEqualTo(MIN)
                .LessThanOrEqualTo(LARGEUR)
                .WithErrorCode("InvalidYLargeur")
                .WithMessage($"Le X et la largeur doit être compris entre {MIN} et {LARGEUR}");

        }
    }
}
