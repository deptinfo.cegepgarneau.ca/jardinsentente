﻿using Application.Interfaces.Services.Plan;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using Web.Features.Plan.CreatePlan;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Plan.EditPlan
{
    public class EditPlanEndPoint : Endpoint<EditPlanRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IPlanUpdateService _planRepository;


        public EditPlanEndPoint(IMapper mapper, IPlanUpdateService planRepository)
        {
            _mapper = mapper;
            _planRepository = planRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Put("plan/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(EditPlanRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Domain.Entities.Plan.Plan>(req);

            await _planRepository.UpdatePLan(request);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
