﻿using FastEndpoints;
using FluentValidation;
using Microsoft.IdentityModel.Tokens;

namespace Web.Features.Reintialiser.Modification
{
    public class ModifcationUserLotValidator : Validator<ModifcationUserLotRequest>
    {
        public ModifcationUserLotValidator()
        {
            RuleFor(x => x.MemberId)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("L'id du membre ne doit pas être nulle");

        }
    }
}
