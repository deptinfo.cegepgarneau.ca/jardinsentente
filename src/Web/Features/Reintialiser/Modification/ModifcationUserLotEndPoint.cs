﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using Web.Features.Regles.GetRegle;
using Web.Features.Regles;
using IMapper = AutoMapper.IMapper;
using FastEndpoints;
using Domain.Entities;
using Org.BouncyCastle.Ocsp;

namespace Web.Features.Reintialiser.Modification
{
    public class ModifcationUserLotEndPoint:Endpoint<ModifcationUserLotRequest,SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _membreRepository;

        public ModifcationUserLotEndPoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _membreRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Put("reintialiser");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(ModifcationUserLotRequest request, CancellationToken ct)
        {
            //var regle = _mapper.Map<Regle>(req);
            await _membreRepository.AssignerLotDebutDanne(request.MemberId,request.numeroDeLot);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
            // var regle = _regleRepository.FindById(request.Id);
            // await SendOkAsync(_mapper.Map<RegleDto>(regle), ct);
        }
    }
}
