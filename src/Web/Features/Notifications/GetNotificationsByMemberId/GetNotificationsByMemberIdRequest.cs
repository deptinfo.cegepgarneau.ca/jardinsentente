﻿namespace Web.Features.Notifications.GetNotificationsByMemberId
{
    public class GetNotificationsByMemberIdRequest
    {
        public Guid? memberId { get; set; }
    }
}
