﻿using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;
using Application.Interfaces.Services.Notifications;
using Domain.Entities.Notifications;
using Domain.Repositories;
using Web.Features.Lots.GetLot;
using Web.Features.Lots;
using Web.Features.Events;
using Web.Features.Events.GetEvent;

namespace Web.Features.Notifications.GetNotificationsByMemberId
{
    public class GetNotificationsByMemberIdEndPoint : Endpoint<GetNotificationsByMemberIdRequest, List<NotificationDto>> { 
        private readonly IMapper _mapper;
        private readonly INotificationRepository _notificationRepository;

        public GetNotificationsByMemberIdEndPoint(IMapper mapper, INotificationRepository notificationRepository)
        {
            _mapper = mapper;
            _notificationRepository = notificationRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("/notification/{memberId}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetNotificationsByMemberIdRequest request, CancellationToken ct)
        {
            var notifs = _notificationRepository.GetNotificationsByMemberId(request.memberId);
            await SendOkAsync(_mapper.Map<List<NotificationDto>>(notifs), ct);
        }
    }
}
