﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Events.GetEvent;

namespace Web.Features.Notifications.GetNotificationsByMemberId
{
    public class GetNotificationsByMemberIdValidator : Validator<GetNotificationsByMemberIdRequest>
    {
        public GetNotificationsByMemberIdValidator()
        {
            RuleFor(x => x.memberId)
                .NotEmpty()
                .WithErrorCode("EmptyMemberId")
                .WithMessage("L'identifiant du membre ne doit pas être nulle");
                
        }
    }
}
