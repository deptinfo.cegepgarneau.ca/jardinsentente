﻿namespace Web.Features.Notifications.CreateNotification
{
    public class CreateNotificationRequest
    {
        public string Title { get; set; } = default!;
        public string Message { get; set; } = default!;
        public Guid ToMemberId { get; set; }
    }
}
