﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Notifications.CreateNotification
{
    public class CreateNotificationValidator : Validator<CreateNotificationRequest>
    {
        public CreateNotificationValidator() 
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidFrenchTitle")
                .WithMessage("Le titre de la notification ne peut pas être nulle ou vide.");

            RuleFor(x => x.Message)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDescription")
                .WithMessage("Le message de la notification ne peut pas être nulle ou vide.");
        }
    }
}
