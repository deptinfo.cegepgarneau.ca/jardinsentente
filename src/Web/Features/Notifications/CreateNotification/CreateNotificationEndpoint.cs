﻿using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;
using Application.Interfaces.Services.Notifications;
using Domain.Entities.Notifications;

namespace Web.Features.Notifications.CreateNotification
{
    public class CreateNotificationEndpoint : Endpoint<CreateNotificationRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly INotificationCreateService _notificationRepository;


        public CreateNotificationEndpoint(IMapper mapper, INotificationCreateService notificationRepository)
        {
            _mapper = mapper;
            _notificationRepository = notificationRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("notification");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateNotificationRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Notification>(req);

            await _notificationRepository.CreateNotification(request);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
