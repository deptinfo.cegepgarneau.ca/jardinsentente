﻿namespace Web.Features.Notifications
{
    public class NotificationDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string ToMemberId { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
