﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Admins.Members.GetAllAdmin
{
    public class GetAllAdminEndpoint : EndpointWithoutRequest<List<MemberDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetAllAdminEndpoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("admins");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var admins = _memberRepository.GetAllAdmins();
            await SendOkAsync(_mapper.Map<List<MemberDto>>(admins), ct);
        }
    }
}
