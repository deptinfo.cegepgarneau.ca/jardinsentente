﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Admins.Members.DeleteMember
{
    public class DeleteMemberEndPoint : Endpoint<DeleteMemberRequest, EmptyResponse>
    {
        private readonly IMemberRepository _memberRepository;

        public DeleteMemberEndPoint(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("members/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteMemberRequest request, CancellationToken ct)
        {
            await _memberRepository.DeleteMemberWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}

