﻿using Application.Interfaces.Services.Members;
using Application.Services.Members;
using Domain.Entities;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Admins.Members.CreateMember
{
    public class CreateMemberEndPoint : Endpoint<CreateMemberRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMemberCreationService _memberCreationService;

        public CreateMemberEndPoint(IMapper mapper, IMemberCreationService memberCreationService)
        {
            _mapper = mapper;
            _memberCreationService = memberCreationService;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("admin/members");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateMemberRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Member>(req);
            await _memberCreationService.CreateMember(req.FirstName, req.LastName, req.Email, req.Password, req.Apartment, req.Street, req.City, req.ZipCode, req.PhoneNumber, req.Lot);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
