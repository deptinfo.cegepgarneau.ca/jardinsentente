﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Admins.Members.CreateMember
{
    public class CreateMemberValidator : Validator<CreateMemberRequest>
    {
        public CreateMemberValidator() 
        {
            RuleFor(x => x.FirstName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidFirstName")
                .WithMessage("Member first name should not be empty or null.");

            RuleFor(x => x.LastName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidLastName")
                .WithMessage("Member last name should not be empty or null.");

            RuleFor(x => x.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress()
                .WithErrorCode("InvalidEmail")
                .WithMessage("Member email should not be empty or null and should be a valid email address.");

            RuleFor(x => x.Password)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidPassword")
                .WithMessage("Member password should not be empty or null.");

            RuleFor(x => x.Street)
                .NotNull()
                .NotEmpty()
                .Length(3, 255)
                .WithErrorCode("InvalidAdress")
                .WithMessage("L'adresse doit contenir entre 3 et 255 caractères");

            RuleFor(x => x.ZipCode)
                .NotNull()
                .NotEmpty()
                .Matches(@"^[A-Z]\d[A-Z] *\d[A-Z]\d$")
                .WithErrorCode("InvalidPostalCode")
                .WithMessage("Veuillez entrer un code postal valide");

            RuleFor(x => x.PhoneNumber)
                .NotNull()
                .NotEmpty()
                .Matches(@"^\d{10}$")
                .WithErrorCode("InvalidPhoneNumber")
                .WithMessage("Veuillez entrer un numéro de téléphone valide");
        }

    }
}

