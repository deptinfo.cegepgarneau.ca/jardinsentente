﻿using Domain.Entities.Lots;

namespace Web.Features.Admins.Members.CreateMember
{
    public class CreateMemberRequest
    {
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public int? Apartment { get; set; }
        public string? Street { get; set; }
        public string? City { get; set; }
        public string? ZipCode { get; set; }
        public string? PhoneNumber { get; set; }
        public Lot Lot {  get; set; }

    }
}
