﻿using Domain.Entities;
using Domain.ValueObjects;
using NodaTime;
using Web.Features.Lots;

namespace Web.Features.Admins.Members;

public class MemberDto
{
    public Guid Id { get; set; }
    public string FirstName { get; private set; } = default!;
    public string LastName { get; private set; } = default!;
    public PhoneNumber? PhoneNumber { get; set; }
    public int? Apartment { get; private set; }
    public string? Street { get; private set; }
    public string? City { get; private set; }
    public string? ZipCode { get; private set; }
    public bool Active { get; private set; }
    public List<string> Roles { get; set; } = default!;
    public List<LotDto> Lots { get; set; } = new List<LotDto>();
    public string Email { get; private set; } = default!;

}