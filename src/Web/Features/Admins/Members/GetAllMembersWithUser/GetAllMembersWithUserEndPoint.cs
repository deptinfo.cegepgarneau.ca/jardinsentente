﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Admins.Members.GetAllMembersWithUser
{
    public class GetAllMembersWithUserEndPoint: EndpointWithoutRequest<List<MemberDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetAllMembersWithUserEndPoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("membersusers");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var members = _memberRepository.GetAllMembersWithUser();
            await SendOkAsync(_mapper.Map<List<MemberDto>>(members), ct);
        }
    }
}
