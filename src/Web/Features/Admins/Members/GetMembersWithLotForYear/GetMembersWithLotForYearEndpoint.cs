﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Admins.Members.GetMembersWithLotForYear
{
    public class GetMembersWithLotForYearEndpoint : EndpointWithoutRequest<List<MemberDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetMembersWithLotForYearEndpoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("members/lot");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var year = _memberRepository.GetMembersWithLotForCurrentYear();
            await SendOkAsync(_mapper.Map<List<MemberDto>>(year), ct);
        }
    }
}
