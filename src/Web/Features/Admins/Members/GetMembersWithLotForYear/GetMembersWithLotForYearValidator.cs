﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Admins.Members.GetMembersWithLotForYear
{
    public class GetMembersWithLotForYearValidator : Validator<GetMembersWithLotForYearRequest>
    {
        public GetMembersWithLotForYearValidator()
        {
            RuleFor(x => x.Year)
                .Empty()
                .WithMessage("Year is required.")
                .Must(year => int.TryParse(year.ToString(), out _))
                .WithMessage("Year is an integer.");
        }
    }
}
