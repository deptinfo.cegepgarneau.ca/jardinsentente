﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Admins.Members;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Admins.Members.LireTousUtilisateurs
{
    public class LireLesMembresEndpoint : EndpointWithoutRequest<List<MemberDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public LireLesMembresEndpoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("members");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var lesMembres = _memberRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<MemberDto>>(lesMembres), ct);
        }
    }
}
