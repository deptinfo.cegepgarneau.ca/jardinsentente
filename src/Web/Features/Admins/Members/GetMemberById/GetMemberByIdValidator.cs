﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Admins.Members.GetMemberById
{
    public class GetMemberByIdValidator: Validator<GetMemberByIdRequest>
    {
        public GetMemberByIdValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Member must have an id");

        }
    }
}
