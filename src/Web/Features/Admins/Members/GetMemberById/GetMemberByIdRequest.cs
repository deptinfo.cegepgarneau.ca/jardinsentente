﻿namespace Web.Features.Admins.Members.GetMemberById
{
    public class GetMemberByIdRequest
    {
        public Guid Id { get; set; }
    }
}
