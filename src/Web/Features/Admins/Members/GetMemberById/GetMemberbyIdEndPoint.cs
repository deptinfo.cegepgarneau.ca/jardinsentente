﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Admins.Members.GetMemberById
{
    public class GetMemberbyIdEndPoint: Endpoint<GetMemberByIdRequest, MemberDto>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetMemberbyIdEndPoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("member/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMemberByIdRequest request, CancellationToken ct)
        {
            var member = _memberRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<MemberDto>(member), ct);
        }
    }
}
