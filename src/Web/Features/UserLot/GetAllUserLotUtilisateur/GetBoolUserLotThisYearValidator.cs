﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.UserLot.GetAllUserLotUtilisateur
{
    public class GetBoolUserLotThisYearValidator: Validator<GetBoolUserLotThisYearRequest>
    {
        public GetBoolUserLotThisYearValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyUserLotId")
                .WithMessage("L'id de l'User-Lot  ne doit pas être nulle");

        }
    }
}
