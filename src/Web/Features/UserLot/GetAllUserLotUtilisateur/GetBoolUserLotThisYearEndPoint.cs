﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Regles.GetRegle;
using Web.Features.Regles;
using FastEndpoints;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.UserLot.GetAllUserLotUtilisateur
{
    public class GetBoolUserLotThisYearEndPoint: Endpoint<GetBoolUserLotThisYearRequest, bool>
    {
        private readonly IMapper _mapper;
        private readonly IUserLotRepository _userLotRepository;

        public GetBoolUserLotThisYearEndPoint(IMapper mapper, IUserLotRepository regleRepository)
        {
            _mapper = mapper;
            _userLotRepository = regleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("userlot/{id}");
           // Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetBoolUserLotThisYearRequest request, CancellationToken ct)
        {
            bool userLot = _userLotRepository.UserLotIsTheYear(request.Id);
            
            await SendOkAsync(userLot, ct);
        }
    }
}
