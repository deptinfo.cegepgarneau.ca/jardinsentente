﻿using Domain.Entities.Identity;
using Domain.Entities.Lots;
using Web.Features.Admins.Members;
using Web.Features.Lots;

namespace Web.Features.UserLot
{
    public class UserLotDto
    {
        public Guid Id { get; set; }
      //  public Guid IdLot { get; set; } = default!;
        public int YearAssignment { get; set; }= default!;
        public int LotNumber { get; set; } = default!;

    }
}
