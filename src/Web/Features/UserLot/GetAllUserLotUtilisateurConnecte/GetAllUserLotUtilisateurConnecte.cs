﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;
using Web.Features.Regles;

namespace Web.Features.UserLot.GetAllUserLotUtilisateurConnecte
{
    public class GetAllUserLotUtilisateurConnecte:EndpointWithoutRequest<List<UserLotDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUserLotRepository _userLotRepository;

        public GetAllUserLotUtilisateurConnecte(IMapper mapper, IUserLotRepository userLotRepository)
        {
            _mapper = mapper;
            _userLotRepository = userLotRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("userlot");
            //Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var userlot = _userLotRepository.GetAllUserLotUtilisateurConnecte();
            await SendOkAsync(_mapper.Map<List<UserLotDto>>(userlot), ct);
        }

    }
}
