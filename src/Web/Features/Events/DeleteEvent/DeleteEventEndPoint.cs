﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Events.DeleteEvent;

namespace Web.Features.Events.DeleteEvent
{
    public class DeleteEventEndPoint : Endpoint<DeleteEventRequest, EmptyResponse>
    {
        private readonly IEventRepository _eventRepository;

        public DeleteEventEndPoint(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("events/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteEventRequest request, CancellationToken ct)
        {
            await _eventRepository.DeleteEventWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
