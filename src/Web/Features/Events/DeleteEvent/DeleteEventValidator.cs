﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Regles.DeleteRegle;

namespace Web.Features.Events.DeleteEvent
{
    public class DeleteEventValidator : Validator<DeleteEventRequest>
    {
        public DeleteEventValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyEventId")
                .WithMessage("Event id is required.");
        }
    }
}
