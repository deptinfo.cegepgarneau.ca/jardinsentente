﻿namespace Web.Features.Events.DeleteEvent
{
    public class DeleteEventRequest
    {
        public Guid Id { get; set; }
    }
}
