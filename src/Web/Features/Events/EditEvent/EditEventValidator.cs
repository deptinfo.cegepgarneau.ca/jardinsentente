﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Regles.EditRegle;

namespace Web.Features.Events.EditEvent
{
    public class EditEventValidator : Validator<EditEventRequest>
    {
        public EditEventValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("EmptyEventId")
                .WithMessage("Event id is required.");

            RuleFor(x => x.Month)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidMonth")
                .WithMessage("Month cannot be null or empty");

            RuleFor(x => x.Jour)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidJour")
                .WithMessage("Jour cannot be null or empty");

            RuleFor(x => x.DayOfWeek)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDayOfWeek")
                .WithMessage("DayOfWeek cannot be null or empty");

            RuleFor(x => x.Description)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDescription")
                .MinimumLength(3)
                .MaximumLength(50)
                .WithMessage("Description cannot be null or empty and must be between 3 and 50 characters");
                

                

        }
    }
}
