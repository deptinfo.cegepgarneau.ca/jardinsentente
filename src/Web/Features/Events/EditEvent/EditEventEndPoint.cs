﻿using Domain.Entities;
using Domain.Repositories;
using FastEndpoints;
using Application.Interfaces.Services.Events;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;
using Domain.Entities.Event;

namespace Web.Features.Events.EditEvent
{
    public class EditEventEndPoint : Endpoint<EditEventRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IEventRepository _eventUpdateService;

        public EditEventEndPoint(IMapper mapper, IEventRepository eventUpdateService)
        {
            _mapper = mapper;
            _eventUpdateService = eventUpdateService;
        }

        public override void Configure()
        {
            DontCatchExceptions();


            Put("events/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(EditEventRequest req, CancellationToken ct)
        {
            var @event = _mapper.Map<Event>(req);
            await _eventUpdateService.UpdateEvent(@event);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
