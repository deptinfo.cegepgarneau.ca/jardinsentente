﻿namespace Web.Features.Events.EditEvent
{
    public class EditEventRequest
    {
        public Guid Id { get; set; }
        public string Month { get; set; } = default!;
        public string Jour { get; set; } = default!;
        public string DayOfWeek { get; set; } = default!;
        public string Description { get; set; } = default!;

    }
}
