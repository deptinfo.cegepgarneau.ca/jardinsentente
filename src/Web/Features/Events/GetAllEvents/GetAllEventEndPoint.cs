﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;
using FastEndpoints;

namespace Web.Features.Events.GetAllEvents
{
    public class GetAllEventEndPoint : EndpointWithoutRequest<List<EventDto>>
    {
        private readonly IMapper _mapper;
        private readonly IEventRepository _eventRepository;

        public GetAllEventEndPoint(IMapper mapper, IEventRepository eventRepository)
        {
            _mapper = mapper;
            _eventRepository = eventRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("events");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var events = _eventRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<EventDto>>(events), ct);
        }
    }
}
