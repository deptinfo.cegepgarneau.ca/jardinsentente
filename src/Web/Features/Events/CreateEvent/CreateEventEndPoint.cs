﻿
using Domain.Entities.Event;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Events.CreateEvent
{
    public class CreateEventEndPoint : Endpoint<CreateEventRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IEventRepository _eventRepository;


        public CreateEventEndPoint(IMapper mapper, IEventRepository eventRepository)
        {
            _mapper = mapper;
            _eventRepository = eventRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("events");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateEventRequest req, CancellationToken ct)
        {
            var @event = _mapper.Map<Event>(req);

            await _eventRepository.CreateEvent(@event);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
