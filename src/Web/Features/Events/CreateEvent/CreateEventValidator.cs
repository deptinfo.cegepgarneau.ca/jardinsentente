﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Events.CreateEvent
{
    public class CreateEventValidator: Validator<CreateEventRequest>
    {
        public CreateEventValidator() 
        { 
            RuleFor(x => x.Month)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidMonth")
                .WithMessage("Month cannot be null or empty");

            RuleFor(x => x.Jour)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidJour")
                .WithMessage("Jour cannot be null or empty");

            RuleFor(x => x.DayOfWeek)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDayOfWeek")
                .WithMessage("DayOfWeek cannot be null or empty");

            RuleFor(x => x.Description)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDescription")
                .MinimumLength(3)
                .MaximumLength(25)
                .WithMessage("Description cannot be null or empty and must be between 3 and 25 characters");
                
        }
    }
}
