﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Events.GetEvent
{
    public class GetEventValidator: Validator<GetEventRequest>
    {
        public GetEventValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyEventId")
                .WithMessage("Event id is required.");
        }
    }
}
