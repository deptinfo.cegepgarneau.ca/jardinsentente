﻿namespace Web.Features.Events.GetEvent
{
    public class GetEventRequest
    {
        public Guid Id { get; set; }
    }
}
