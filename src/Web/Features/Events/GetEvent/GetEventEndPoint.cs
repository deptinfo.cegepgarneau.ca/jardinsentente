﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Events.GetEvent
{
    public class GetEventEndPoint: Endpoint<GetEventRequest, EventDto>
    {
        private readonly IMapper _mapper;
        private readonly IEventRepository _eventRepository;

        public GetEventEndPoint(IMapper mapper, IEventRepository eventRepository)
        {
            _mapper = mapper;
            _eventRepository = eventRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("events/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetEventRequest request, CancellationToken ct)
        {
            var @event = _eventRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<EventDto>(@event), ct);
        }
    }
}
