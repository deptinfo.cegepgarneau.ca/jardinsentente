﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Regles.CreateRegle
{
    public class CreateRegleValidator : Validator<CreateRegleRequest>
    {
        public CreateRegleValidator()
        {
            RuleFor(x => x.TitreFr)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidFrenchTitle")
                .WithMessage("Le titre de la regle ne peut pas être nulle ou vide");

            RuleFor(x => x.DescriptionFr)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDescription")
                .WithMessage("La description de la regle ne peut pas être nulle ou vide");
        }
    }
}
