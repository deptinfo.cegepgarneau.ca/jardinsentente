﻿using Application.Interfaces.Services.Books;
using Application.Interfaces.Services.Regles;
using Domain.Entities;
using Domain.Entities.Books;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Regles.CreateRegle
{
    public class CreateRegleEndPoint: Endpoint<CreateRegleRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRegleCreationService _regleRepository;


        public CreateRegleEndPoint(IMapper mapper, IRegleCreationService regleRepository)
        {
            _mapper = mapper;
            _regleRepository = regleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("regles");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateRegleRequest req, CancellationToken ct)
        {
            var request = _mapper.Map<Regle>(req);

            await _regleRepository.CreateRegle(request);

            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
