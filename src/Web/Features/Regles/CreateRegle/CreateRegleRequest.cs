﻿namespace Web.Features.Regles.CreateRegle
{
    public class CreateRegleRequest
    {
        public string TitreFr { get; set; } = default!;
        public string DescriptionFr { get; set; } = default!;
    }
}
