﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Regles.DeleteRegle
{
    public class DeleteRegleValidator: Validator<DeleteRegleRequest>
    {
        public DeleteRegleValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Regle id is required.");
        }
    }
}
