﻿namespace Web.Features.Regles.DeleteRegle
{
    public class DeleteRegleRequest
    {
        public Guid Id { get; set; }
    }
}
