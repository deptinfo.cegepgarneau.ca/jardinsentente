﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Regles.DeleteRegle
{
    public class DeleteRegleEndPoint:Endpoint<DeleteRegleRequest,EmptyResponse>
    {
        private readonly IRegleRepository _regleRepository;

        public DeleteRegleEndPoint(IRegleRepository regleRepository)
        {
            _regleRepository = regleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("regles/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteRegleRequest request, CancellationToken ct)
        {
            await _regleRepository.DeleteRegleWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
