﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Regles.GetRegle
{
    public class GetRegleEndpoint: Endpoint<GetRegleRequest, RegleDto>
    {
        private readonly IMapper _mapper;
        private readonly IRegleRepository _regleRepository;

        public GetRegleEndpoint(IMapper mapper, IRegleRepository regleRepository)
        {
            _mapper = mapper;
            _regleRepository = regleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("regles/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetRegleRequest request, CancellationToken ct)
        {
            var regle = _regleRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<RegleDto>(regle), ct);
        }
    }
}
