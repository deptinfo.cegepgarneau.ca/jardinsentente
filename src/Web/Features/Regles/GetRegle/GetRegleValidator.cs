﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Regles.GetRegle
{
    public class GetRegleValidator: Validator<GetRegleRequest>
    {
        public GetRegleValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("L'id de la règle ne doit pas être nulle");

        }

    }
}
