﻿using Domain.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;
using FastEndpoints;



namespace Web.Features.Regles.GetAllRegles
{
    public class GetAllReglesEndPoint: EndpointWithoutRequest<List<RegleDto>>
    {
        private readonly IMapper _mapper;
        private readonly IRegleRepository _regleRepository;

        public GetAllReglesEndPoint(IMapper mapper, IRegleRepository regleRepository)
        {
            _mapper = mapper;
            _regleRepository = regleRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("regles");
         //   Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var regles = _regleRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<RegleDto>>(regles), ct);
        }
    }
}
