﻿namespace Web.Features.Regles.EditRegle
{
    public class EditRegleRequest
    {
        public Guid Id { get; set; }
        public string TitreFr { get; set; } = default!;
        public string DescriptionFr { get; set; } = default!;
    }
}
