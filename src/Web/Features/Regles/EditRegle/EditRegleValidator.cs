﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Regles.EditRegle
{
    public class EditRegleValidator: Validator<EditRegleRequest>
    {
        public EditRegleValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("ID vide")
                .WithMessage("L'ID de la regle ne peut pas être nulle ou vide");


            RuleFor(x => x.TitreFr)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidFrenchTitle")
                .WithMessage("Le titre de la regle ne peut pas être nulle ou vide");

            RuleFor(x => x.DescriptionFr)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidDescription")
                .WithMessage("La description de la regle ne peut pas être nulle ou vide");
        }
    }
}
