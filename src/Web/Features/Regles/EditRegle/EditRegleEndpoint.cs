﻿using Application.Interfaces.Services.Books;
using Application.Interfaces.Services.Regles;
using Domain.Entities;
using Domain.Entities.Books;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;


namespace Web.Features.Regles.EditRegle
{
    public class EditRegleEndpoint: Endpoint<EditRegleRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRegleUpdateService _regleUpdateService;

        public EditRegleEndpoint(IMapper mapper, IRegleUpdateService regleUpdateService)
        {
            _mapper = mapper;
            _regleUpdateService = regleUpdateService;
        }

        public override void Configure()
        {
            DontCatchExceptions();


            Put("regles/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(EditRegleRequest req, CancellationToken ct)
        {
            var regle = _mapper.Map<Regle>(req);
            await _regleUpdateService.UpdateRegle(regle);
            await SendOkAsync(new SucceededOrNotResponse(true), ct);
        }
    }
}
