﻿namespace Web.Features.Regles
{
    public class RegleDto
    {
        public Guid Id { get; set; }
        public string TitreFr { get; set; } = default!;
        public string DescriptionFr { get; set; } = default!;
        public string Slug { get; set; } = default!;
    }
}
