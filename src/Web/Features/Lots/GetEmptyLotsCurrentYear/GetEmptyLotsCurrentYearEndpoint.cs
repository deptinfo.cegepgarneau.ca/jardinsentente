﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Events;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Lots.GetLot
{
    public class GetEmptyLotsCurrentYearEndpoint : EndpointWithoutRequest<List<LotDto>>
    {
        private readonly IMapper _mapper;
        private readonly ILotRepository _lotRepository;

        public GetEmptyLotsCurrentYearEndpoint(IMapper mapper, ILotRepository lotRepository)
        {
            _mapper = mapper;
            _lotRepository = lotRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("empty/lots");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var emptyLot = _lotRepository.GetLotsEmptyCurrentYear();
            await SendOkAsync(_mapper.Map<List<LotDto>>(emptyLot), ct);
        }
    }
}
