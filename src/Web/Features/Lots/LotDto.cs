﻿namespace Web.Features.Lots
{
    public class LotDto
    {
        public Guid Id { get; set; } = default!;
        public string Address { get; set; } = default!;
        public double Temperature { get; set; } = default!;
        public double Humidity { get; set; } 
        public double SunlightIntensity { get; set; }
        public int LotNumber { get; private set; }

    }
}
