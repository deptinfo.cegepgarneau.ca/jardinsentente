﻿namespace Web.Features.Lots.GetLot
{
    public class GetEmptyLotsCurrentYearRequest
    {
        public int lotNumber { get; set; }
    }
}
