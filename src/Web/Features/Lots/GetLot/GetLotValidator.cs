﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Lots.GetLot

{
    public class GetLotValidator: Validator<GetEmptyLotsCurrentYearRequest>
    {
        public GetLotValidator()
        {
            RuleFor(x => x.lotNumber)
                .NotEmpty()
                .WithErrorCode("EmptyLotNumber")
                .WithMessage("Le numéro de lot ne doit pas être nulle");
        }   
    }
}
