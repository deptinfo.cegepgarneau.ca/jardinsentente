﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Lots.GetLot
{
    public class GetLotEndpoint : Endpoint<GetEmptyLotsCurrentYearRequest, LotDto>
    {
        private readonly IMapper _mapper;
        private readonly ILotRepository _lotRepository;

        public GetLotEndpoint(IMapper mapper, ILotRepository lotRepository)
        {
            _mapper = mapper;
            _lotRepository = lotRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("lot/{lotNumber}");
            //Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetEmptyLotsCurrentYearRequest request, CancellationToken ct)
        {
            var book = _lotRepository.FindByLotNumber(request.lotNumber);
            await SendOkAsync(_mapper.Map<LotDto>(book), ct);
        }
    }
}
