﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Invite.WaitingMembers;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.WaitingMembers.GetAllWaitingMembers
{
    public class GetAllWaitingMembersEndpoint : EndpointWithoutRequest<List<WaitingMemberDto>>
    {
        private readonly IMapper _mapper;
        private readonly IWaitingMemberRepository _waitingMemberRepository;

        public GetAllWaitingMembersEndpoint(IMapper mapper, IWaitingMemberRepository waitingMemberRepository)
        {
            _mapper = mapper;
            _waitingMemberRepository = waitingMemberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("waitingMembers/all");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var waitingMembers = _waitingMemberRepository.GetAll();
            await SendOkAsync(_mapper.Map<List<WaitingMemberDto>>(waitingMembers), ct);
        }
    }
}
