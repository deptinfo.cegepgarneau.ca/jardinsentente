﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Regles.DeleteRegle;

namespace Web.Features.WaitingMembers.DeleteWaitingMember
{
    public class DeleteWaitingMemberEndPoint : Endpoint<DeleteWaitingMemberRequest, EmptyResponse>
    {
        private readonly IWaitingMemberRepository _waitingMemberRepository;

        public DeleteWaitingMemberEndPoint(IWaitingMemberRepository waitingMemberRepository)
        {
            _waitingMemberRepository = waitingMemberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("waitingMember/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteWaitingMemberRequest request, CancellationToken ct)
        {
            await _waitingMemberRepository.DeleteWaitingMemberWithId(request.Id);
            await SendNoContentAsync(ct);
        }
    }
}
