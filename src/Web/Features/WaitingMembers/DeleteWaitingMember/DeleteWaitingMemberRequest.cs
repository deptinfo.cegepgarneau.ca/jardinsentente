﻿namespace Web.Features.WaitingMembers.DeleteWaitingMember
{
    public class DeleteWaitingMemberRequest
    {
        public Guid Id { get; set; }
    }
}
