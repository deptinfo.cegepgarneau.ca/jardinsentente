﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Regles.DeleteRegle;

namespace Web.Features.WaitingMembers.DeleteWaitingMember
{
    public class DeleteWaitingMemberValidator : Validator<DeleteWaitingMemberRequest>
    {
        public DeleteWaitingMemberValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyWaitingMemberId")
                .WithMessage("Waiting member id is required.");
        }
    }

}
