﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.WaitingMembers.GetWaitingMember
{
    public class GetWaitingMemberValidator: Validator<GetWaitingMemberRequest>
    {
        public GetWaitingMemberValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyRegleId")
                .WithMessage("Waiting member must have an id");

        }
    }
}
