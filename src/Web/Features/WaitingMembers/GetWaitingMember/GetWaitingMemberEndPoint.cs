﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Invite.WaitingMembers;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.WaitingMembers.GetWaitingMember
{
    public class GetWaitingMemberEndPoint : Endpoint<GetWaitingMemberRequest, WaitingMemberDto>
    {
        private readonly IMapper _mapper;
        private readonly IWaitingMemberRepository _waitingMemberRepository;

        public GetWaitingMemberEndPoint(IMapper mapper, IWaitingMemberRepository waitingMemberRepository)
        {
            _mapper = mapper;
            _waitingMemberRepository = waitingMemberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("waitingMembers/{id}");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetWaitingMemberRequest request, CancellationToken ct)
        {
            var waitingMember = _waitingMemberRepository.FindById(request.Id);
            await SendOkAsync(_mapper.Map<WaitingMemberDto>(waitingMember), ct);
        }
    }
}
