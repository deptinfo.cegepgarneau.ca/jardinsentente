﻿namespace Web.Features.WaitingMembers.GetWaitingMember
{
    public class GetWaitingMemberRequest
    {
        public Guid Id { get; set; }
    }
}
