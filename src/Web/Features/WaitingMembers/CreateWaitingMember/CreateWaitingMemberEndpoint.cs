using Application.Interfaces.Services.WaitingMembers;
using Domain.Entities.WaitingMembers;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using Web.Features.WaitingMembers.CreateWaitingMember;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.WaitingMembers.CreateWaitingMember;

public class CreateWaitingMemberEndpoint : Endpoint<CreateWaitingMemberRequest, SucceededOrNotResponse>
{
    private readonly IMapper _mapper;
    private readonly IWaitingMemberCreationService _waitingMemberCreationService;

    public CreateWaitingMemberEndpoint(IMapper mapper, IWaitingMemberCreationService waitingMemberCreationService)
    {
        _mapper = mapper;
        _waitingMemberCreationService = waitingMemberCreationService;
    }

    public override void Configure() 
    {
        AllowFileUploads();
        DontCatchExceptions();
        Post("waitingMembers");
        Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public override async Task HandleAsync(CreateWaitingMemberRequest req, CancellationToken ct) 
    {
        var waitingMember = _mapper.Map<WaitingMember>(req);
        await _waitingMemberCreationService.CreateWaitingMember(waitingMember);
        await SendOkAsync(new SucceededOrNotResponse(true), ct);
    }
}