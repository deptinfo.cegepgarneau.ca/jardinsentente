using FastEndpoints;
using FluentValidation;
using Web.Extensions;


namespace Web.Features.WaitingMembers.CreateWaitingMember;

public class CreateWaitingMemberValidator : Validator<CreateWaitingMemberRequest>
{
    public CreateWaitingMemberValidator() 
    {
        RuleFor(x => x.FirstName)
            .NotNull()
            .NotEmpty()
            .Length(3,50)
            .WithErrorCode("InvalidFirstName")
            .WithMessage("Le pr�nom doit contenir entre 3 et 50 caract�res");
        RuleFor(x => x.LastName)
            .NotEmpty()
            .NotNull()
            .Length(3,50)
            .WithErrorCode("InvalidLastName")
            .WithMessage("Le nom doit contenir entre 3 et 50 caract�res");
        RuleFor(x => x.Adress)
            .NotNull()
            .NotEmpty()
            .Length(3,255)
            .WithErrorCode("InvalidAdress")
            .WithMessage("L'adresse doit contenir entre 3 et 255 caract�res");
        RuleFor(x => x.PostalCode)
            .NotNull()
            .NotEmpty()
            .Matches(@"^[A-Z]\d[A-Z] *\d[A-Z]\d$")
            .WithErrorCode("InvalidPostalCode")
            .WithMessage("Veuillez entrer un code postal valide");
        RuleFor(x => x.Email)
            .NotNull()
            .NotEmpty()
            .Matches(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
            .WithErrorCode("InvalidEmail")
            .WithMessage("Veuillez entrer une adresse courriel valide");
        RuleFor(x => x.PhoneNumber)
            .NotNull()
            .NotEmpty()
            .Matches(@"^\d{10}$")
            .WithErrorCode("InvalidPhoneNumber")
            .WithMessage("Veuillez entrer un num�ro de t�l�phone valide");
    }
}