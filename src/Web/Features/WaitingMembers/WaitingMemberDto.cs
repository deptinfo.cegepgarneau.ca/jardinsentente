namespace Web.Features.Invite.WaitingMembers;

public class WaitingMemberDto
{
    public Guid Id { get; set; }
    public DateTime Created { get; set; }
    public string FirstName { get; set; } = default!;
    public string LastName { get; set; } = default!;
    public string Adress { get; set; } = default!;
    public int? AppNumber { get; set; }
    public string PostalCode { get; set; } = default!;
    public string PhoneNumber { get; set; } = default!;
    public string Email { get; set; } = default!;
    public string From { get; set; } = default!;
}