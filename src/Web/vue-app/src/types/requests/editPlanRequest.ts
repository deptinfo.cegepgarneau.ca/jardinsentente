export interface IEditPlanRequest {
    id?: string;
    nom?: string;
    x?: string;
    y?: string;
    longueur?: string;
    largeur?: string;
}