export interface IEditEventRequest {
    id?: string
    month?: string
    jour?: string
    dayOfWeek?: string
    description?: string
}