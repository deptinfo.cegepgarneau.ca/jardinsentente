export interface ICreateEventRequest {
    month?: string
    jour?: string
    dayOfWeek?: string
    description?: string
}
