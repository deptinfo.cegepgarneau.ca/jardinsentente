export interface IEditRegleRequest {
    id?: string
    titreFr?: string
    descriptionFr?: string
}