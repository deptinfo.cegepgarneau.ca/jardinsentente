export interface ICreateWaitingMemberRequest {
    firstName?: string;
    lastName?: string;
    adress?: string;
    appNb?: number;
    postalCode?: string;
    phoneNumber?: string;
    email?: string;
}