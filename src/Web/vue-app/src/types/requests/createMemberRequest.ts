export interface ICreateMemberRequest {
    firstName?: string;
    lastName?: string;
    email?: string;
    password?: string;
    role?: string;
    apartment?: number;
    street?: string;
    city?: string;
    zipCode?: string;
    phoneNumber?: string;
}