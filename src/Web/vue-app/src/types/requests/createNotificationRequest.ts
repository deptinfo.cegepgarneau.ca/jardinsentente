export interface ICreateNotificationRequest {
    title?: string;
    message?: string;
    toMemberId?: string;
}