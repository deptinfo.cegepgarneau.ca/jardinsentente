export interface ICreatePlanRequest {
    nom?: string;
    x?: string;
    y?: string;
    longueur?: string;
    largeur?: string;
}