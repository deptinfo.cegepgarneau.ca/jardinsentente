export interface IAuthenticatedMember {
  id? : string  
  firstName: string
  lastName: string
  jobTitle?: string
  email?: string
  phoneNumber?: string
  phoneExtension?: number
  apartment?: number
  street?: string
  city?: string
  zipCode?: string
  roles: string[]
  organizationId: string
  enterpriseNumber?: string
  memberNumber?: string
}