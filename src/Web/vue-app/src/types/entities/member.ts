import {IPerson} from "@/types/entities/person";
import { Lot } from "./lot";

export class Member implements IPerson {
  id?: string
  crmId?: number
  firstName?: string
  lastName?: string
  jobTitle?: string
  email?: string
  phoneNumber?: string
  phoneExtension?: number
  apartment?: number
  street?: string
  city?: string
  zipCode?: string
  userId?: string
  organizationId?: string
  roles?: string[]
  lots?: Lot[]
}