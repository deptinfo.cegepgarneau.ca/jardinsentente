export class Event {
    id?: string;
    month?: string;
    jour?: string;
    dayOfWeek?: string;
    description?: string;
}