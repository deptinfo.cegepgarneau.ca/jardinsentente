﻿export class Mailing {
    recipients?: string[];
    subject?: string;
    message?: string;
}
