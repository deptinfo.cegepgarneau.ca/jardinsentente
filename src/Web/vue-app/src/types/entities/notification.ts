import { DateTime } from "luxon";

export class Notification {
    id?: string;
    title?: string;
    message?: string;
    toMemberId?: string;
    createAt?: DateTime;
}