export class WaitingMember {
    id?: string;
    firstName?: string;
    lastName?: string;
    adress?: string;
    appNb?: number;
    postalCode?: string;
    phoneNumber?: string;
    email?: string;
    from?: string;
}