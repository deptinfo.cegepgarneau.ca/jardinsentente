import i18n from "@/i18n";
import { Role } from "@/types";
import { useMemberStore } from "@/stores/memberStore";
import { createRouter, createWebHistory } from "vue-router";

import monJardin from "../views/Lot/MonJardin.vue";
import AdminUserIndex from "@/views/admin/AdminUserIndex.vue"


const router = createRouter({
    // eslint-disable-next-line
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 };
    },
    history: createWebHistory(),
    routes: [
        {
            // path: i18n.t("routes.home.path"),
            path: i18n.t("routes.historique.path"),
            name: "historiques",
            component: () => import('../views/Home.vue'),
        },
        {
            path: i18n.t("routes.home.path"),
            // path: i18n.t("routes.lot.path"),
            name: "Mon Jardin",
            component: monJardin,
            meta: {
                requiredRole: Role.User,
                noLinkInBreadcrumbs: true,
            },
        },
        {
            path: i18n.t("routes.regles.path"),
            name: "regles",
            component: () => import('../views/Regle/RegleClient.vue'),
        },
        {
            path: i18n.t("routes.planindex.path"),
            name: "plan index",
            component: () => import('../views/UserLot/PlanIndex.vue'),
            props: true
        },
        {
            path: i18n.t("routes.inscription.path"),
            name: "inscription",
            component: () => import('../views/Inscription.vue'),
            meta: {
                requiredRole: Role.Admin,
                noLinkInBreadcrumbs: true,
            },
        },
        {
            path: i18n.t("routes.admin.path"),
            name: "admin",
            component: () => import('../views/admin/Admin.vue'),
            meta: {
                requiredRole: Role.Admin,
                noLinkInBreadcrumbs: true,
            },
            children: [
                {
                    path: i18n.t("routes.admin.children.users.path"),
                    name: "admin.children.users",
                    component: () => import('../views/admin/Admin.vue'),
                    children: [
                        {
                            path: "",
                            name: "admin.children.users.index",
                            component: AdminUserIndex ,
                        },
                        {
                            path: i18n.t("routes.admin.children.users.add.path"),
                            name: "admin.children.users.children.add",
                            component: () => import('@/views/admin/AdminCreateMember.vue'),
                            props: true
                        },
                        {
                            path: i18n.t("routes.admin.children.users.notification.path"),
                            name: "admin.children.users.children.notification",
                            component: () => import('@/views/admin/AdminNotifierMember.vue'),
                            props: true
                        }
                    ],
                },
                {
                    path: i18n.t("routes.admin.children.mailing.path"),
                    name: "admin.children.mailing",
                    component: () => import('../views/admin/Admin.vue'),
                    children: [
                        {
                            path: "",
                            name: "admin.children.mailing.index",
                            component: () => import('../views/admin/AdminMailingForm.vue'),
                        }
                    ],
                },
                {
                    path: i18n.t("routes.admin.children.regles.path"),
                    name: "admin.children.regles",
                    component: () => import('../views/admin/Admin.vue'),
                    children: [
                        {
                            path: "",
                            name: "admin.children.regles.index",
                            component: () => import('../views/Regle/RegleIndex.vue'),
                        },

                        {
                            path: i18n.t("routes.admin.children.regles.add.path"),
                            name: "admin.children.regles.children.add",
                            component: () => import('../views/Regle/RegleAjout.vue'),
                        },
                        {
                            path: i18n.t("routes.admin.children.regles.visualiser.path"),
                            name: "admin.children.regles.children.visualiser",
                            component: () => import('../views/Regle/RegleVisualiser.vue'),
                            props: true
                        },
                        {
                            path: i18n.t("routes.admin.children.regles.edit.path"),
                            name: "admin.children.regles.children.edit",
                            component: () => import('../views/Regle/RegleEdit.vue'),
                            props: true
                        },
                    ],

                },
                {
                    path: i18n.t("routes.admin.children.events.path"),
                    name: "admin.children.events",
                    component: () => import('../views/admin/Admin.vue'),
                    children: [
                        {
                            path: "",
                            name: "admin.children.events.index",
                            component: () => import('../views/Event/EventIndex.vue'),
                        },
                        {
                            path: i18n.t("routes.admin.children.events.add.path"),
                            name: "admin.children.events.add",
                            component: () => import('../views/Event/EventAjout.vue'),
                        },
                        {
                            path: i18n.t("routes.admin.children.events.edit.path"),
                            name: "admin.children.events.edit",
                            component: () => import('../views/Event/EventEdit.vue'),
                            props: true
                        },
                    ],
                },
                {
                    path: i18n.t("routes.admin.children.reintialiser.path"),
                    name: "admin.children.reintialiser",
                    component: () => import('../views/Reintialiser/reintialiser.vue'),
                },
            ]
        },
        {
            path: '/:notFound(.*)',
            name: 'NotFound',
            component: () => import('../views/NotFound/NotFound.vue')
        },
    ]
});

// eslint-disable-next-line
router.beforeEach(async (to, from) => {
    const memberStore = useMemberStore()

    if (!to.meta.requiredRole)
        return;
    const isRoleArray = Array.isArray(to.meta.requiredRole)
    const doesNotHaveGivenRole = !isRoleArray && !memberStore.hasRole(to.meta.requiredRole as Role);
    const hasNoRoleAmountRoleList = isRoleArray && !memberStore.hasOneOfTheseRoles(to.meta.requiredRole as Role[]);
    if (doesNotHaveGivenRole || hasNoRoleAmountRoleList) {
        return {
            name: "regles",
        };

        //if (memberStore.hasRole(Role.User))
        //    return {
        //        name: "Mon Jardin",
        //    };
        //else
        //    return {
        //        name: "admin.children.users.index",
        //    };
    }
});

export const Router = router;