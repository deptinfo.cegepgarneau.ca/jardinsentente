import { defineStore } from 'pinia'
import { UserLot } from "@/types/entities";
//import { IAuthenticatedMember } from "@/types/entities/authenticatedMember";
//import { Role } from "@/types";

interface MemberState {
    userLot: UserLot[],
  //  modification: boolean
}

export const useUserLotStore = defineStore('userLot', {
    state: (): MemberState => ({
        userLot: [] as UserLot[],
      //  modification: false
        
    }),

    actions: { // methods
        setUserlot(userlot: UserLot[]) {
            this.userLot = userlot
        },
        //setmodification(valeur: boolean) {
        //    this.modification = valeur
        //},
        //hasRole(role: Role): boolean {
        //    return this.member.roles.some(x => x === role)
        //},
        //hasOneOfTheseRoles(roles: Role[]): boolean {
        //    return roles.some(x => this.member.roles.includes(x));
        //}
    },

    getters: { // computed
        getUserlot: (state) => state.userLot,
        getIdUserlotYear: (state) => state.userLot.find(u => u.yearAssignment == (new Date().getFullYear()).toString())?.id ?? "",
       // getModification: (state) => state.modification
    },

    persist: {
        storage: sessionStorage
    }
})