import {Container} from "inversify";
import axios, {AxiosInstance} from 'axios';
import "reflect-metadata";

import {TYPES} from "@/injection/types";
import {
  IApiService,
  IBookService,
    ILotService,
    IMemberService,
    IRegleService,
    IUserLotService,
    IPlanService,
    IWaitingMemberService,
    INotificationService,
    IEventService,
    IMailingService,
    IReintialiserService
} from "@/injection/interfaces";

import {
  ApiService,
  BookService,
  MemberService,
    RegleService,
    UserLotService,
    PlanService,
    LotService,
    EventService,
    ReintialiserService
   
} from "@/services";
import { WaitingMemberService } from "./services/waitingMemberService";
import { MailingService } from "./services/mailingService";
import { NotificationService } from "./services/notificationService";

const dependencyInjection = new Container();
dependencyInjection.bind<AxiosInstance>(TYPES.AxiosInstance).toConstantValue(axios.create())
dependencyInjection.bind<IApiService>(TYPES.IApiService).to(ApiService).inSingletonScope()
dependencyInjection.bind<IMemberService>(TYPES.IMemberService).to(MemberService).inSingletonScope()
dependencyInjection.bind<IBookService>(TYPES.IBookService).to(BookService).inSingletonScope()
dependencyInjection.bind<IRegleService>(TYPES.IRegleService).to(RegleService).inSingletonScope()
dependencyInjection.bind<IWaitingMemberService>(TYPES.IWaitingMemberService).to(WaitingMemberService).inSingletonScope()
dependencyInjection.bind<INotificationService>(TYPES.INotificationService).to(NotificationService).inSingletonScope()
dependencyInjection.bind<IEventService>(TYPES.IEventService).to(EventService).inSingletonScope()
dependencyInjection.bind<ILotService>(TYPES.ILotService).to(LotService).inSingletonScope()
dependencyInjection.bind<IUserLotService>(TYPES.IUserLotService).to(UserLotService).inSingletonScope()
dependencyInjection.bind<IPlanService>(TYPES.IPlanService).to(PlanService).inSingletonScope()
dependencyInjection.bind<IMailingService>(TYPES.IMailingService).to(MailingService).inSingletonScope()
dependencyInjection.bind<IReintialiserService>(TYPES.IReintialiserService).to(ReintialiserService).inSingletonScope()

function useMemberService() {
  return dependencyInjection.get<IMemberService>(TYPES.IMemberService);
}

function useBookService() {
  return dependencyInjection.get<IBookService>(TYPES.IBookService);
}

function useEventService() {
    return dependencyInjection.get<IEventService>(TYPES.IEventService);
}

function useRegleService() {
    return dependencyInjection.get<IRegleService>(TYPES.IRegleService);
}

function useLotService() {
    return dependencyInjection.get<ILotService>(TYPES.ILotService);
}


function useWaitingMemberService() { 
    return dependencyInjection.get<IWaitingMemberService>(TYPES.IWaitingMemberService)
}

function useUserLotService() {
    return dependencyInjection.get<IUserLotService>(TYPES.IUserLotService)
}

function usePLanService() {
    return dependencyInjection.get<IPlanService>(TYPES.IPlanService)
}

function useMailingService() {
    return dependencyInjection.get<IMailingService>(TYPES.IMailingService)
}

function useNotificationService() {
    return dependencyInjection.get<INotificationService>(TYPES.INotificationService)
}

function useReintialiserService() {
    return dependencyInjection.get<IReintialiserService>(TYPES.IReintialiserService)
}

export {
  dependencyInjection,
  useMemberService,
    useBookService,
    useRegleService,
    useLotService,
    useUserLotService,
    usePLanService,
    useEventService,
  useWaitingMemberService,
    useMailingService,
    useNotificationService,
    useReintialiserService
};