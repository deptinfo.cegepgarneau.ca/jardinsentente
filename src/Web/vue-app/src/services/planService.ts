import { IPlanService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { AxiosError, AxiosResponse } from "axios";
import { PLan } from "../types/entities/plan";
import { SucceededOrNotResponse } from "@/types/responses";
import { ICreatePlanRequest, IEditPlanRequest } from "../types/requests";

@injectable()
export class PlanService extends ApiService implements IPlanService {
    public async deleteRegle(idPlan: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/plan/${idPlan}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(response.status === 204, succeededOrNotResponse.errors)
    }
    public async editPlan(request: IEditPlanRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .put<IEditPlanRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/plan/${request.id}`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async getPLan(idPlan: string): Promise<PLan> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<PLan>>(`${process.env.VUE_APP_API_BASE_URL}/api/plan/${idPlan}`)
            .catch(function (error: AxiosError): AxiosResponse<PLan> {
                return error.response as AxiosResponse<PLan>;
            });
        return response.data as PLan
    }

    public async LireTousPlanWithIdUserLot(userLotId: string): Promise<PLan[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<PLan[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/plan/userlot/${userLotId}`)
            .catch(function (error: AxiosError): AxiosResponse<PLan[]> {
                return error.response as AxiosResponse<PLan[]>
            })
        return response.data as PLan[]
    }

    public async createPlan(request: ICreatePlanRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreatePlanRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/plan`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

}