import { IRegleService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { ICreateRegleRequest } from "@/types/requests/createRegleRequest";
import { Regle } from "@/types/entities";
import { IEditRegleRequest } from "@/types/requests/editRegleRequest";

@injectable()
export class RegleService extends ApiService implements IRegleService {
    public async getAllRegles(): Promise<Regle[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Regle[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/regles`)
            .catch(function (error: AxiosError): AxiosResponse<Regle[]> {
                return error.response as AxiosResponse<Regle[]>
            })
        return response.data as Regle[]
    }

    public async getRegle(regleId: string): Promise<Regle> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Regle>>(`${process.env.VUE_APP_API_BASE_URL}/api/regles/${regleId}`)
            .catch(function (error: AxiosError): AxiosResponse<Regle> {
                return error.response as AxiosResponse<Regle>;
            });
        return response.data as Regle
    }

    public async deleteRegle(regleId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/regles/${regleId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async createRegle(request: ICreateRegleRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateRegleRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/regles`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async editRegle(request: IEditRegleRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .put<ICreateRegleRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/regles/${request.id}`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}