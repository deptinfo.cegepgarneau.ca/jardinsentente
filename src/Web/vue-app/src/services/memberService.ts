import { injectable } from "inversify"
import { AxiosError, AxiosResponse } from "axios";

import "@/extensions/date.extensions"
import { ApiService } from "@/services/apiService"
import { IMemberService } from "@/injection/interfaces"
import { IAuthenticatedMember } from "@/types/entities/authenticatedMember"
import { Member } from "../types/entities"
import { SucceededOrNotResponse } from "../types/responses";
import { ICreateMemberRequest } from "@/types/requests/createMemberRequest";

@injectable()
export class MemberService extends ApiService implements IMemberService {
    public async activerMemberDebutDanne(memberId: string, numeroDeLot: number): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .put<any, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/reintialiser`,
                { numeroDeLot, memberId },
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async getCurrentMember(): Promise<IAuthenticatedMember | undefined> {
        try {
            const response = await this
                ._httpClient
                .get<IAuthenticatedMember, AxiosResponse<IAuthenticatedMember>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/me`)
            return response.data
        } catch (error) {
            return Promise.reject(error)
        }
    }

    public async getAllMembers(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/members`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async getAllMembersWithLotThisYear(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/lot`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async getAllAdmins(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/admins`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async deleteMember(memberId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse> {
        console.log(request.street)
        const response = await this
            ._httpClient
            .post<ICreateMemberRequest, AxiosResponse<SucceededOrNotResponse>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/admin/members`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            });
        const succeededOrNotResponse = response.data as SucceededOrNotResponse;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)

    }
    public async getAllMembersWithUser(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/membersusers`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async getMemberById(memberId: string): Promise<Member> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member>>(`${process.env.VUE_APP_API_BASE_URL}/api/member/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<Member> {
                return error.response as AxiosResponse<Member>;
            });
        return response.data as Member
    }
}


