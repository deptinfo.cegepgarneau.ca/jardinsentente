import { ILotService } from '@/injection/interfaces'; 
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { Lot } from "@/types/entities";

@injectable()

export class LotService extends ApiService implements ILotService {
    public async getAllNumeroLotDisponible(): Promise<number[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<number[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/lot`)
            .catch(function (error: AxiosError): AxiosResponse<number[]> {
                return error.response as AxiosResponse<number[]>;
            });
        return response.data as number[]
    }
    public async getLot(lotNumber: number): Promise<Lot> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Lot>>(`${process.env.VUE_APP_API_BASE_URL}/api/lot/${lotNumber}`)
            .catch(function (error: AxiosError): AxiosResponse<Lot> {
                return error.response as AxiosResponse<Lot>;
            });
        return response.data as Lot
    }

    public async getLotsEmptyCurrentYear(): Promise<Lot[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Lot>>(`${process.env.VUE_APP_API_BASE_URL}/api/empty/lots`)
            .catch(function (error: AxiosError): AxiosResponse<Lot> {
                return error.response as AxiosResponse<Lot>;
            });
        return response.data as Lot[]
    }
}