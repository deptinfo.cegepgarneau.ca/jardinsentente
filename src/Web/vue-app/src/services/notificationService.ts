import { INotificationService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { AxiosError, AxiosResponse } from "axios";
import { SucceededOrNotResponse } from "../types/responses";
import { ICreateNotificationRequest } from "@/types/requests/createNotificationRequest";

@injectable()
export class NotificationService extends ApiService implements INotificationService
{
    public async createNotification(request: ICreateNotificationRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateNotificationRequest, AxiosResponse<SucceededOrNotResponse>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/notification`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            });
        const succeededOrNotResponse = response.data as SucceededOrNotResponse;
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async GetNotificationsByMemberId(memberId?: string): Promise<Notification[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Notification[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/notification/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<Notification[]> {
                return error.response as AxiosResponse<Notification[]>
            });
        return response.data as Notification[];
    }

}