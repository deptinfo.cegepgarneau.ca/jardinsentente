import { IReintialiserService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";


@injectable()
export class ReintialiserService extends ApiService implements IReintialiserService {
    public async reintialiserUser(): Promise<SucceededOrNotResponse> {

        const response = await this
            ._httpClient
            .post<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/reintialiser`,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}