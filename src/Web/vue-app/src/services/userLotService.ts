import { IUserLotService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { AxiosError, AxiosResponse } from "axios";
import { UserLot } from "../types/entities/userLot";

@injectable()
export class UserLotService extends ApiService implements IUserLotService {
    public async GetBoolUserLotThisYear(userId: string): Promise<boolean> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<boolean>>(`${process.env.VUE_APP_API_BASE_URL}/api/userlot/${userId}`)
            .catch(function (error: AxiosError): AxiosResponse<boolean> {
                return error.response as AxiosResponse<boolean>
            })
        return response.data as boolean
    }

    public async LireTousUserLotUtilisateurConnecte(): Promise<UserLot[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<UserLot[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/userlot`)
            .catch(function (error: AxiosError): AxiosResponse<UserLot[]> {
                return error.response as AxiosResponse<UserLot[]>
            })
        return response.data as UserLot[]
    }

}