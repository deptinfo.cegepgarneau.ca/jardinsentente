import { IWaitingMemberService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { ICreateWaitingMemberRequest } from "../types/requests/createWaitingMemberRequest";
import { WaitingMember } from "@/types/entities/waitingMember";

@injectable()
export class WaitingMemberService extends ApiService implements IWaitingMemberService {

    public async createWaitingMember(request: ICreateWaitingMemberRequest): Promise<SucceededOrNotResponse> {
        console.log(request)
        const response = await this
            ._httpClient
            .post<ICreateWaitingMemberRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/waitingMembers`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async getAllWaitingMembers(): Promise<WaitingMember[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<WaitingMember[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/waitingMembers/all`)
            .catch(function (error: AxiosError): AxiosResponse<WaitingMember[]> {
                return error.response as AxiosResponse<WaitingMember[]>
            })
        return response.data as WaitingMember[]
    }

    public async getWaitingMemberById(waitingMemberId: string): Promise<WaitingMember> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<WaitingMember>>(`${process.env.VUE_APP_API_BASE_URL}/api/waitingMembers/${waitingMemberId}`)
            .catch(function (error: AxiosError): AxiosResponse<WaitingMember> {
                return error.response as AxiosResponse<WaitingMember>;
            });
        return response.data as WaitingMember
    }

    public async deleteWaitingMember(waitingMemberId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/waitingMember/${waitingMemberId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }
}