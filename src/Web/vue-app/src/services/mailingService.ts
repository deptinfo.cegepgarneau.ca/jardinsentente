﻿import { injectable, inject } from "inversify"
import { AxiosError, AxiosInstance, AxiosResponse } from "axios";
import { SucceededOrNotResponse } from "../types/responses";
import { ApiService } from "@/services/apiService"
import { Mailing } from "../types/entities/mailing";
import { useMemberService, useWaitingMemberService } from "@/inversify.config";
import { Member } from "@/types/entities";
import { WaitingMember } from "@/types/entities/waitingMember";
import { IMailingService } from "../injection/interfaces";

@injectable()
export class MailingService extends ApiService implements IMailingService{
    memberService = useMemberService()
    waitingMemberService = useWaitingMemberService()

    public async getMembersAndWaitingMembers(): Promise<{ members: Member[], waitingMembers: WaitingMember[]} > {

        const members = await this.memberService.getAllMembersWithLotThisYear();

        const waitingMembers = await this.waitingMemberService.getAllWaitingMembers();

        return { members, waitingMembers }
    }

    public async sendMail(request: Mailing): Promise<SucceededOrNotResponse> {
        try {
            const response = await this._httpClient.post<Mailing, AxiosResponse<SucceededOrNotResponse>>(
                `${process.env.VUE_APP_API_BASE_URL}/mailing/send`,
                request,
                { headers: { 'Content-Type': 'application/json' } }
            );

            const succeededOrNotResponse = response.data;
            return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors);
        } catch(error: any) {
            console.error('Error send mail: ', error)
            return new SucceededOrNotResponse(false, [error.message])
        }
    }
}
