import { IEventService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { ICreateEventRequest } from "@/types/requests/createEventRequest";
import { Event } from "@/types/entities";
import { IEditEventRequest } from "@/types/requests/editEventRequest";

@injectable()
export class EventService extends ApiService implements IEventService {
    public async getAllEvents(): Promise<Event[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Event[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/events`)
            .catch(function (error: AxiosError): AxiosResponse<Event[]> {
                return error.response as AxiosResponse<Event[]>
            })
        return response.data as Event[]
    }

    public async getEvent(eventId: string): Promise<Event> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Event>>(`${process.env.VUE_APP_API_BASE_URL}/api/events/${eventId}`)
            .catch(function (error: AxiosError): AxiosResponse<Event> {
                return error.response as AxiosResponse<Event>;
            });
        return response.data as Event
    }

    public async deleteEvent(eventId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/events/${eventId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async createEvent(request: ICreateEventRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateEventRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/events`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async editEvent(request: IEditEventRequest): Promise<SucceededOrNotResponse> {
        console.log(request.id)
        const response = await this
            ._httpClient
            .put<IEditEventRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/events/${request.id}`,
                request,
                this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                console.log(error)
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}