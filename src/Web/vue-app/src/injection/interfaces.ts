// eslint-disable-next-line @typescript-eslint/no-empty-interface
import {
  ICreateBookRequest,
    IEditBookRequest,
    ICreateRegleRequest,
    IEditRegleRequest,
    ICreateEventRequest,
    IEditEventRequest,
    ICreatePlanRequest,
    IEditPlanRequest
} from "@/types/requests"
import {SucceededOrNotResponse} from "@/types/responses"
import { ICreateWaitingMemberRequest } from "../types/requests/createWaitingMemberRequest"
import { ICreateMemberRequest } from "../types/requests/createMemberRequest"
import { ICreateNotificationRequest } from "../types/requests/createNotificationRequest"
import { WaitingMember } from "../types/entities/waitingMember"
import { Book, IAuthenticatedMember, Lot, Member, Regle, PLan, Event } from "@/types/entities"
import { UserLot } from "../types/entities/userLot"
import { Mailing } from "../types/entities/mailing"

export interface IApiService {
  headersWithJsonContentType(): any

  headersWithFormDataContentType(): any

  buildEmptyBody(): string
}

export interface IMemberService {
    getCurrentMember(): Promise<IAuthenticatedMember | undefined>

    getAllMembers(): Promise<Member[]>

    getAllMembersWithLotThisYear(): Promise<Member[]>

    getAllAdmins(): Promise<Member[]>

    deleteMember(memberId: string): Promise<SucceededOrNotResponse>
    createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse>

    getAllMembersWithUser(): Promise<Member[]>

    activerMemberDebutDanne(memberId: string, numeroDeLot: number): Promise<SucceededOrNotResponse>
    getMemberById(memberId: string): Promise<Member>
}

export interface IBookService {
  getAllBooks(): Promise<Book[]>

  getBook(bookId: string): Promise<Book>

  deleteBook(bookId: string): Promise<SucceededOrNotResponse>

  createBook(request: ICreateBookRequest): Promise<SucceededOrNotResponse>

  editBook(request: IEditBookRequest): Promise<SucceededOrNotResponse>
}

export interface IRegleService {
    getAllRegles(): Promise<Regle[]>

    getRegle(regleId: string): Promise<Regle>

    deleteRegle(regleId: string): Promise<SucceededOrNotResponse>

    createRegle(request: ICreateRegleRequest): Promise<SucceededOrNotResponse>

    editRegle(request: IEditRegleRequest): Promise<SucceededOrNotResponse>
}

export interface IEventService {
    getAllEvents(): Promise<Event[]>

    getEvent(eventId: string): Promise<Event>

    deleteEvent(eventId: string): Promise<SucceededOrNotResponse>

    createEvent(request: ICreateEventRequest): Promise<SucceededOrNotResponse>

    editEvent(request: IEditEventRequest): Promise<SucceededOrNotResponse>
}


export interface ILotService {
    getLot(lotNumber: number): Promise<Lot>
    getLotsEmptyCurrentYear(): Promise<Lot[]>
}  


export interface IWaitingMemberService {
    createWaitingMember(request: ICreateWaitingMemberRequest): Promise<SucceededOrNotResponse>
    getAllWaitingMembers(): Promise<WaitingMember[]>
    getWaitingMemberById(waitingMemberId: string): Promise<WaitingMember>
    deleteWaitingMember(waitingMemberId: string): Promise<SucceededOrNotResponse>
}

export interface IUserLotService {
    GetBoolUserLotThisYear(userId: string): Promise<boolean>
    LireTousUserLotUtilisateurConnecte(): Promise<UserLot[]>
}

export interface INotificationService {
    createNotification(request: ICreateNotificationRequest): Promise<SucceededOrNotResponse>
    GetNotificationsByMemberId(memberId?: string): Promise<Notification[]>
}

export interface IPlanService {
    LireTousPlanWithIdUserLot(userLotId: string): Promise<PLan[]>
    createPlan(request: ICreatePlanRequest): Promise<SucceededOrNotResponse>
    getPLan(idPlan: string): Promise<PLan>
    editPlan(request: IEditPlanRequest): Promise<SucceededOrNotResponse>
    deleteRegle(idPlan: string): Promise<SucceededOrNotResponse>
}

export interface IMailingService {
    getMembersAndWaitingMembers(): Promise<{ members: Member[], waitingMembers: WaitingMember[] }>
    sendMail(request: Mailing): Promise<SucceededOrNotResponse>
}

export interface IReintialiserService {
    reintialiserUser() :Promise<SucceededOrNotResponse>
}