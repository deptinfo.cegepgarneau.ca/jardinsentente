export const TYPES = {
  IApiService: Symbol.for("IApiService"),
  IMemberService: Symbol.for("IMemberService"),
  IBookService: Symbol.for("IBookService"),
    AxiosInstance: Symbol.for("AxiosInstance"),
    IRegleService: Symbol.for("IRegleService"),
    IEventService: Symbol.for("IEventService"),
    IWaitingMemberService: Symbol.for("IWaitingMemberService"),
  INotificationService: Symbol.for("INotificationService"),
  ILotService: Symbol.for("ILotService"),
    IUserLotService: Symbol.for("IUserLotService"),
    IPlanService: Symbol.for("IPlanService"),
    IMailingService: Symbol.for("IMailingService"),
    IReintialiserService: Symbol.for("IReintialiserService")
};
