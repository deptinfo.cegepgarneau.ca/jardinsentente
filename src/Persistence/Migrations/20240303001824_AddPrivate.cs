﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    public partial class AddPrivate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLot_AspNetUsers_UserId",
                table: "UserLot");

            migrationBuilder.DropForeignKey(
                name: "FK_UserLot_Lots_LotId",
                table: "UserLot");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLot",
                table: "UserLot");

            migrationBuilder.RenameTable(
                name: "UserLot",
                newName: "UserLots");

            migrationBuilder.RenameIndex(
                name: "IX_UserLot_UserId",
                table: "UserLots",
                newName: "IX_UserLots_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserLot_LotId",
                table: "UserLots",
                newName: "IX_UserLots_LotId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLots",
                table: "UserLots",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLots_AspNetUsers_UserId",
                table: "UserLots",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserLots_Lots_LotId",
                table: "UserLots",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLots_AspNetUsers_UserId",
                table: "UserLots");

            migrationBuilder.DropForeignKey(
                name: "FK_UserLots_Lots_LotId",
                table: "UserLots");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLots",
                table: "UserLots");

            migrationBuilder.RenameTable(
                name: "UserLots",
                newName: "UserLot");

            migrationBuilder.RenameIndex(
                name: "IX_UserLots_UserId",
                table: "UserLot",
                newName: "IX_UserLot_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserLots_LotId",
                table: "UserLot",
                newName: "IX_UserLot_LotId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLot",
                table: "UserLot",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLot_AspNetUsers_UserId",
                table: "UserLot",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserLot_Lots_LotId",
                table: "UserLot",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
