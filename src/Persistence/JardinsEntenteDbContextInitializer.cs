using Domain.Constants.User;
using Domain.Entities;
using Domain.Entities.Event;
using Domain.Entities.Lots;
using Domain.Entities.Identity;
using Domain.Entities.WaitingMembers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Domain.Entities.Plan;
using Persistence.Migrations;

namespace Persistence;

public class JardinsEntenteDbContextInitializer
{
    const string DEFAULT_EMAIL = "admin@gmail.com";

    private readonly ILogger<JardinsEntenteDbContextInitializer> _logger;
    private readonly JardinsEntenteDbContext _context;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<User> _userManager;

    public JardinsEntenteDbContextInitializer(ILogger<JardinsEntenteDbContextInitializer> logger,
        JardinsEntenteDbContext context,
        RoleManager<Role> roleManager,
        UserManager<User> userManager)
    {
        _logger = logger;
        _context = context;
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    public async Task SeedAsync()
    {
        try
        {
            await SeedRoles();
            await SeedUsersAndMembersForRole(Roles.ADMINISTRATOR);
            await SeedRegle();
            await SeedMembers();
            await SeedWaitingMembers();
            await SeedEvents();
            await SeedLots();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }

    private async Task SeedRoles()
    {
        
        if (!_roleManager.RoleExistsAsync(Roles.ADMINISTRATOR).Result)
            await _roleManager.CreateAsync(new Role { Name = Roles.ADMINISTRATOR, NormalizedName = Roles.ADMINISTRATOR.Normalize() });
        if (!_roleManager.RoleExistsAsync(Roles.USER).Result)
            await _roleManager.CreateAsync(new Role { Name = Roles.USER, NormalizedName = Roles.USER.Normalize() });
    }

    private async Task SeedUsersAndMembersForRole(string role)
    {
        var user = await _userManager.FindByEmailAsync(DEFAULT_EMAIL);
        if (user == null)
        {
            user = BuildUser();
            var result = await _userManager.CreateAsync(user, "Qwerty123!");

            if (result.Succeeded)
                await _userManager.AddToRoleAsync(user, role);
            else
                throw new Exception($"Could not seed/create {role} user.");
        }

        var existingMember = _context.Members.IgnoreQueryFilters().FirstOrDefault(x => x.User.Id == user.Id);
        if (existingMember is { Active: true })
            return;

        if (existingMember == null)
        {
            var member = new Member("ADMIN", "MEMBER", null, "123, my street", "my city", "A1A 1A1");
            member.SetUser(user);
            _context.Members.Add(member);
            await _context.SaveChangesAsync();
        }
        else if (!existingMember.Active)
        {
            existingMember.Activate();
            _context.Members.Update(existingMember);
            await _context.SaveChangesAsync();
        }
    }

    private User BuildUser()
    {
        return new User
        {
            Email = DEFAULT_EMAIL,
            UserName = DEFAULT_EMAIL,
            NormalizedEmail = DEFAULT_EMAIL.Normalize(),
            NormalizedUserName = DEFAULT_EMAIL,
            PhoneNumber = "555-555-5555",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            TwoFactorEnabled = false
        };
    }

 

    
    

    private async Task SeedRegle()
    {
        List<Regle> seedRegles = new List<Regle>();
        
        Regle regle1= new Regle();
        regle1.SetTitre("Titre1");
        regle1.SetDescription("Description1");
        regle1.SetSlug("Slug1");

        Regle regle2 = new Regle();
        regle2.SetTitre("Titre2");
        regle2.SetDescription("Description2");
        regle2.SetSlug("Slug2");

        Regle regle3 = new Regle();
        regle3.SetTitre("Titre3");
        regle3.SetDescription("Description3");
        regle3.SetSlug("Slug3");

        Regle regle4 = new Regle();
        regle4.SetTitre("Titre4");
        regle4.SetDescription("Description4");
        regle4.SetSlug("Slug4");

        seedRegles.Add(regle1);
        seedRegles.Add(regle2);
        seedRegles.Add(regle3);
        seedRegles.Add(regle4);

        var existingRegle = _context.Regles.IgnoreQueryFilters().FirstOrDefault(x => x.TitreFr == seedRegles[0].TitreFr);
        if (existingRegle != null)
            return;
        _context.Regles.AddRange(seedRegles);
        await _context.SaveChangesAsync();
    }
    
    
    private async Task SeedMember(string email, string userName, string firstName, string street, string city, string postalCode, 
        string adress, double temperature, double humidity, double sunlightIntensity, int lotNumber)
    {
        var user = await _userManager.FindByEmailAsync(email);
        if (user == null)
        {
            user = new User
            {
                Email = email,
                UserName = email,
                NormalizedEmail = email.Normalize(),
                NormalizedUserName = email,
                PhoneNumber = "555-555-5555",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                TwoFactorEnabled = false
            };
            var result = await _userManager.CreateAsync(user, "Qwerty123!");
            if (result.Succeeded)
                await _userManager.AddToRoleAsync(user, Roles.USER);
            else
                throw new Exception($"Could not seed/create {Roles.USER} user.");
        }

        var existingMember = _context.Members.IgnoreQueryFilters().FirstOrDefault(x => x.User.Id == user.Id);
        if (existingMember is { Active: true })
            return;

        if (existingMember == null)
        {
            var member = new Member(userName, firstName, null, street, city, postalCode);
            member.SetUser(user);
            member.Activate();
            _context.Members.Add(member);
           await _context.SaveChangesAsync();
        }
        else if (!existingMember.Active)
        {
            existingMember.Activate();
            _context.Members.Update(existingMember);
            await _context.SaveChangesAsync();
        }
        
        Lot lot = new Lot(adress, temperature, humidity, sunlightIntensity, lotNumber);
        var existingLots = _context.Lots.IgnoreQueryFilters().FirstOrDefault(x => x.LotNumber == lot.LotNumber);
        if (existingLots != null)
            return;
        _context.Lots.AddRange(lot);
        await _context.SaveChangesAsync();
        Console.WriteLine("Seed: " + lot.Id);

        UserLot userLot = new UserLot();
        userLot.SetLot(lot);
        userLot.SetUser(user);
        userLot.SetYearAssignment(2024);
     /*   var existingUserLot = _context.UserLots.IgnoreQueryFilters().FirstOrDefault(x => x.Lot.Id == userLot.Lot.Id && x.User.Id == userLot.User.Id && x.YearAssignment == userLot.YearAssignment);
        if (existingUserLot != null)
            return;*/
        UserLot userLot2 = new UserLot();
        userLot2.SetLot(lot);
        userLot2.SetUser(user);
        userLot2.SetYearAssignment(2023);
       /* var existingUserLot2 = _context.UserLots.IgnoreQueryFilters().FirstOrDefault(x => x.Lot.Id == userLot2.Lot.Id && x.User.Id == userLot2.User.Id && x.YearAssignment == userLot2.YearAssignment);
       if (existingUserLot2 != null)
            return;*/
        _context.UserLots.AddRange(new List<UserLot>() { userLot, userLot2 });
       await _context.SaveChangesAsync();

        Plan plan1 = new Plan();
        plan1.SetNom("Tomate");
        plan1.SetUserLot(userLot);
        plan1.SetX(0);
        plan1.SetLargeur(1);
        plan1.SetLongueur(2);
        plan1.SetY(0);

        Plan plan2 = new Plan();
        plan2.SetUserLot(userLot);
        plan2.SetX(110);
        plan2.SetLargeur(1);
        plan2.SetLongueur(2);
        plan2.SetNom("L�gume");
        plan2.SetY(110);
        _context.Plans.AddRange(new List<Plan>() { plan1, plan2 });
        await _context.SaveChangesAsync();

    }

    private async Task SeedMembers()
    {
        await SeedMember("user1@gmail.com", "Banane", "Dupuis", "rue des Bananes", "Qu�bec", "G1A1A1", "1660 Boulevard de l'Entente", 36, 56, 1223, 1);
        await SeedMember("user2@gmail.com", "Pomme", "Tremblay", "rue des Pommes", "Qu�bec", "G1B1B1", "1660 Boulevard de l'Entente", 14, 12, 1273, 2);
        await SeedMember("user3@gmail.com", "Carotte", "Gagnon", "rue des Carottes", "Qu�bec", "G1C1C1", "1660 Boulevard de l'Entente", 12, 34, 1269, 3);
        await SeedMember("user4@gmail.com", "Poire", "Fortin", "rue des Poires", "Qu�bec", "G1D1D1", "1660 Boulevard de l'Entente", 27, 20, 1221, 4);
    }

    private async Task SeedLots()
    {
        List<Lot> seedLots = new List<Lot>();
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 4));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 5));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 6));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 7));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 8));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 9));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 10));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 11));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 12));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 13));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 14));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 15));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 16));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 17));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 18));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 19));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 20));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 21));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 22));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 23));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 24));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 25));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 26));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 27));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 28));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 29));
        seedLots.Add(new Lot("1660 Boulevard de l'Entente", 36, 56, 1223, 30));
        foreach (var lot in seedLots)
        {
            var existingLot = _context.Lots.IgnoreQueryFilters().FirstOrDefault(x => x.LotNumber == lot.LotNumber);
            if (existingLot == null)
            {
                _context.Lots.Add(lot);
            }
        }
        await _context.SaveChangesAsync();

    }

    private async Task SeedWaitingMembers()
    {
        if (!_context.WaitingMembers.Any())
        {
            List<WaitingMember> waitingMembers = new List<WaitingMember>();

            WaitingMember waitingM1 = new WaitingMember();
            waitingM1.SetFirstName("John");
            waitingM1.SetLastName("Doe");
            waitingM1.SetAdress("123 Main St");
            waitingM1.SetAppNumber(1);
            waitingM1.SetPostalCode("A1Y2O6");
            waitingM1.SetPhoneNumber("4185206985");
            waitingM1.SetEmail("doe@gmail.com");
            waitingM1.SetFrom("city");
            waitingMembers.Add(waitingM1);

            WaitingMember waitingM2 = new WaitingMember();
            waitingM2.SetFirstName("Alice");
            waitingM2.SetLastName("Smith");
            waitingM2.SetAdress("456 Elm St");
            waitingM2   .SetAppNumber(2);
            waitingM2.SetPostalCode("B2Z3P9");
            waitingM2.SetPhoneNumber("4181234567");
            waitingM2.SetEmail("alice@gmail.com");
            waitingM2.SetEmail("alice@example.com");
            waitingM2.SetFrom("Qu�bec");
            waitingM2.SetFrom("city");
            waitingMembers.Add(waitingM2);


            WaitingMember waitingM3 = new WaitingMember();
            waitingM3.SetFirstName("Bob");
            waitingM3.SetLastName("Johnson");
            waitingM3.SetAdress("789 Oak St");
            waitingM3.SetAppNumber(2);
            waitingM3.SetPostalCode("C3X4Q8");
            waitingM3.SetPhoneNumber("4189876543");
            waitingM3.SetEmail("bob@gmail.com");
            waitingM3.SetFrom("city");
            waitingMembers.Add(waitingM3);

            var existingWaitingList = _context.WaitingMembers.IgnoreQueryFilters().FirstOrDefault(x => x.Email == waitingMembers[0].Email);
            if (existingWaitingList != null)
                return;
            _context.WaitingMembers.AddRange(waitingMembers);
            await _context.SaveChangesAsync();
        }
    }
    private async Task SeedEvents()
    {
        List<Event> events = new List<Event>();
        Event e1 = new Event("Juillet", "1", "Vendredi", "Arroser le lot");
        e1.SetSlug("Slug1");
        Event e2 = new Event("Juillet", "2", "Samedi", "Arroser le lot");
        e2.SetSlug("Slug2");
        Event e3 = new Event("Juillet", "3", "Dimanche", "Arroser le lot");
        e3.SetSlug("Slug3");
        Event e4 = new Event("Juillet", "4", "Lundi", "Arroser le lot");
        e4.SetSlug("Slug4");
        Event e5 = new Event("Juillet", "5", "Mardi", "Arroser le lot");
        e5.SetSlug("Slug5");
       /* Event e6 = new Event("Juillet", "6", "Mercredi", "Arroser le lot");
        e6.SetSlug("Slug6");*/
        events.Add(e1);
        events.Add(e2);
        events.Add(e3);
        events.Add(e4);
        events.Add(e5);
       // events.Add(e6);
        var existingEvents = _context.Events.IgnoreQueryFilters().FirstOrDefault(x => x.Description == events[0].Description);

        if (existingEvents is not null)
            return;
        _context.Events.AddRange(events);
        await _context.SaveChangesAsync();
    }


}
