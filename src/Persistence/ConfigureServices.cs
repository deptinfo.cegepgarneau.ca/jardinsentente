﻿using Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EntityFrameworkCore.SqlServer.NodaTime.Extensions;
using Persistence.Interceptors;

namespace Persistence;

public static class ConfigureServices
{
    public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
    {
        ConfigureInfrastructureServices(services);

        ConfigureDbContext(services, configuration);

        return services;
    }

    public static async Task InitializeAndSeedDatabase(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var initializer = scope.ServiceProvider.GetRequiredService<JardinsEntenteDbContextInitializer>();
        await initializer.InitialiseAsync();
        await initializer.SeedAsync();
    }

    private static void ConfigureInfrastructureServices(IServiceCollection services)
    {
        services.AddScoped<AuditableAndSoftDeletableEntitySaveChangesInterceptor>();
        services.AddScoped<AuditableEntitySaveChangesInterceptor>();
        services.AddScoped<EntitySaveChangesInterceptor>();
        services.AddScoped<UserSaveChangesInterceptor>();
    }

    private static void ConfigureDbContext(IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<JardinsEntenteDbContext>(options =>
        {
            options.UseSqlServer(
                configuration.GetConnectionString("DefaultConnection")!,
                optionsBuilder => optionsBuilder
                    .UseNodaTime()
                    .EnableRetryOnFailure()
                    .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                    .MigrationsAssembly(typeof(JardinsEntenteDbContext).Assembly.FullName));
        });

        services.AddScoped<IJardinsEntenteDbContext>(provider => provider.GetRequiredService<JardinsEntenteDbContext>());
        services.AddScoped<JardinsEntenteDbContextInitializer>();
    }
}
