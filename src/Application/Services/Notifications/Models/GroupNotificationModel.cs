﻿using Application.Services.Notifications.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Notifications.Models
{
    public class GroupNotificationModel : NotificationModel
    {

        public List<string> Recipients { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public string Locale = "fr";

        public GroupNotificationModel(List<string> recipients, string locale, string subject, string message) 
            : base(string.Join(",", recipients), locale)
        {
            if (recipients == null || recipients.Count == 0)
                throw new ArgumentNullException("Avoir au moins un destinataire.");

            Recipients = recipients;
            Locale = locale;
            Subject = subject;
            Message = message;
        }

        public override object TemplateData()
        {
            return new
            {
                Subject,
                Message
            };
        }

        public override string TemplateId()
        {
            if (Locale == "fr")
                return "d-ccea0bf1594048259d41fb52c2c23614";
            return "d-6bceb5f892064a7b95cc03fe16b45943";
        }
    }
}
