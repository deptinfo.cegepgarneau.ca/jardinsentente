using Application.Interfaces.Mailing;
using Application.Interfaces.Services.Notifications;
using Application.Services.Notifications.Dtos;
using Application.Services.Notifications.Models;
using Domain.Entities.Identity;

namespace Application.Services.Notifications;

public class EmailNotificationService : INotificationService
{
    private const string EMAIL_DEFAULT_CULTURE = "fr";

    private readonly IEmailSender _emailSender;

    public EmailNotificationService(IEmailSender emailSender)
    {
        _emailSender = emailSender;
    }

    public async Task<SendNotificationResponseDto> SendForgotPasswordNotification(User user, string link)
    {
        var model = new ForgotPasswordNotificationModel(
            user.Email,
            EMAIL_DEFAULT_CULTURE,
            link);

        return await _emailSender.SendAsync(model);
    }

    public async Task<SendNotificationResponseDto> SendTwoFactorAuthenticationCodeNotification(string destination,
        string code)
    {
        var model = new TwoFactorAuthenticationNotificationModel(
            destination,
            EMAIL_DEFAULT_CULTURE,
            code);

        return await _emailSender.SendAsync(model);
    }

    public async Task<SendNotificationResponseDto> SendGroupNotification(List<string> recipients, string subject, string message)
    {
        var model = new GroupNotificationModel(
            recipients,
            EMAIL_DEFAULT_CULTURE,
            subject,
            message);

        return await _emailSender.SendAsync(model);
    }
}