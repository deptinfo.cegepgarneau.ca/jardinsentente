﻿using Application.Interfaces.FileStorage;
using Application.Interfaces.Services.Plan;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Plan
{
    public class PlanCreationService : IPlanCreationService
    {
        private readonly IPlanRepository _planRepository;

        public PlanCreationService(
            IPlanRepository planRepository)
        {
            _planRepository = planRepository;
        }

        public async Task CreatePlan(Domain.Entities.Plan.Plan plan, Guid userLotId)
        {
            await _planRepository.CreatePlan(plan, userLotId);
        }
    }
}
