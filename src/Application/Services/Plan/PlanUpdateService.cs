﻿using Application.Interfaces.Services.Plan;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Plan
{
    internal class PlanUpdateService:IPlanUpdateService
    {
        private readonly IPlanRepository _planRepository;

        public PlanUpdateService(
            IPlanRepository planRepository)
        {
            _planRepository = planRepository;
        }
        public async Task UpdatePLan(Domain.Entities.Plan.Plan plan)
        {
            await _planRepository.UpdatePLan(plan);
        }
    }
}
