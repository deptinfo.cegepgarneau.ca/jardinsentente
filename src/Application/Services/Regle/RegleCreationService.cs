﻿using Application.Interfaces.Services.Regles;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Regle
{
    public class RegleCreationService : IRegleCreationService
    {
        private readonly IRegleRepository _regleRepository;

        public RegleCreationService(
            IRegleRepository regleRepository)
        {
            _regleRepository = regleRepository;
        }
        public async Task CreateRegle(Domain.Entities.Regle regle)
        {
            await _regleRepository.CreateRegle(regle);
        }
    }
}
