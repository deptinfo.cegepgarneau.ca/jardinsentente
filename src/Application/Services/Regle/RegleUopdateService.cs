﻿using Application.Interfaces.Services.Regles;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Regle
{
    public class RegleUopdateService : IRegleUpdateService
    {
        private readonly IRegleRepository _regleRepository;

        public RegleUopdateService(
            IRegleRepository regleRepository)
        {
            _regleRepository = regleRepository;
        }
        public async Task UpdateRegle(Domain.Entities.Regle regle)
        {
            await _regleRepository.UpdateRegle(regle);
        }
    }
}
