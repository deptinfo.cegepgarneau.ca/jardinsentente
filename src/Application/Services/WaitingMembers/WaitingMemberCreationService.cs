﻿using Application.Interfaces.Services.WaitingMembers;
using Domain.Entities.WaitingMembers;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.WaitingMembers
{
    public class WaitingMemberCreationService : IWaitingMemberCreationService
    {
        private readonly IWaitingMemberRepository _waitingMemberRepository;
        public WaitingMemberCreationService(IWaitingMemberRepository waitingMemberRepository)
        {
            _waitingMemberRepository = waitingMemberRepository;
        }
        public async Task CreateWaitingMember(WaitingMember waitingMember) 
        {
            await _waitingMemberRepository.CreateWaitingMember(waitingMember);
        }
    }
}
