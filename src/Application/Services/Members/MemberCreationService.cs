﻿using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Entities.Lots;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Members
{
    public class MemberCreationService : IMemberCreationService
    {
        private readonly IMemberRepository _memberRepository;

        public MemberCreationService(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public async Task<Member> CreateMember(string firstName, string lastName, string email, string password, int? apartment, string? street, string? city, string? zipCode, string? phoneNumber, Lot lot)
        {
            Member newMember = await _memberRepository.CreateMember(firstName, lastName, email, password, apartment, street, city, zipCode, phoneNumber, lot);
            return newMember;
        }
    }
}
