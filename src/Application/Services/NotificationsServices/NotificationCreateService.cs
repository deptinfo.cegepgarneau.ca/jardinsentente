﻿using Application.Interfaces.Services.Notifications;
using Domain.Entities.Notifications;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.NotificationsServices
{
    public class NotificationCreateService : INotificationCreateService
    {
        private readonly INotificationRepository _notificationRepository;

        public NotificationCreateService(INotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }
        public async Task CreateNotification(Notification notification)
        {
            await _notificationRepository.CreateNotification(notification);
        }
    }
}
