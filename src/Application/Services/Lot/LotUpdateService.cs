﻿using Domain.Repositories;
using Domain.Entities.Lots;
using Application.Interfaces.Services.Lots;

namespace Application.Services.Lots
{
    public class LotUpdateService: ILotUpdateService
    {
        private readonly ILotRepository _lotRepository;

        public LotUpdateService(ILotRepository lotRepository)
        {
            _lotRepository = lotRepository;
        }
        public async Task UpdateLot(int lotNumber, double newTemperature, double newHumidity)
        {
            var lot = _lotRepository.FindByLotNumber(lotNumber);
            lot.SetTemperature(newTemperature);
            lot.SetHumidity(newHumidity);
            await _lotRepository.UpdateLot(lot);
        }
    }
}
