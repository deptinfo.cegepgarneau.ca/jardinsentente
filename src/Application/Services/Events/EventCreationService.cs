﻿using Application.Interfaces.Services.Events;
using Domain.Entities.Event;
using Domain.Repositories;

namespace Application.Services.Events
{
    public class EventCreationService : IEventCreationService
    {
        private readonly IEventRepository _eventRepository;
        

        public EventCreationService(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task CreateEvent(Event @event)
        {
            await _eventRepository.CreateEvent(@event);
        }
    }
}
