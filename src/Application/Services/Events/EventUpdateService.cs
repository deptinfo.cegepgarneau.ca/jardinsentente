﻿using Application.Exceptions.Books;
using Application.Interfaces.FileStorage;
using Application.Interfaces.Services.Events;
using Domain.Entities.Event;
using Domain.Repositories;
using System;

namespace Application.Services.Events
{
    public class EventUpdateService : IEventUpdateService
    {
        private readonly IEventRepository _eventRepository;

        public EventUpdateService(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task UpdateEvent(Event @event)
        {
            var existingEvent = _eventRepository.FindById(@event.Id);
            if (existingEvent == null)
                throw new Exception($"Could not find event with id {@event.Id}.");

            await _eventRepository.UpdateEvent(@event);
        }

        
    }
}
