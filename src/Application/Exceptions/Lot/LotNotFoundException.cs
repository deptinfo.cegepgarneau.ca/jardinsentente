﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Lot
{
    public class LotNotFoundException:Exception
    {
        public LotNotFoundException(string message):base(message) { }

    }
}
