﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Lot
{
    public class UpdateLotException: Exception
    {
        public UpdateLotException(string message) : base(message) { }
    }
}
