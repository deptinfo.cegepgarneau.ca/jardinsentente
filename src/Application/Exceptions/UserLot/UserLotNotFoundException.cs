﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.UserLot
{
    public class UserLotNotFoundException:Exception
    {
        public UserLotNotFoundException(string message):base(message) { }
    }
}
