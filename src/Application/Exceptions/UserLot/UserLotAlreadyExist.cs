﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.UserLot
{
    public class UserLotAlreadyExist:Exception
    {
        public UserLotAlreadyExist(string message):base(message) { }
    }
}
