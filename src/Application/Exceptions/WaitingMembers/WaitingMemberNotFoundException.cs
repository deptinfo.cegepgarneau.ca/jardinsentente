﻿namespace Application.Exceptions.WaitingMembers
{
    public class WaitingMemberNotFoundException: Exception
    {
        public WaitingMemberNotFoundException(string message) : base(message) { }
    }
}
