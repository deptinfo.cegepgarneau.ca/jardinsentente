﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Regles
{
    public class RegleAlreadyExistException:Exception
    {
        public RegleAlreadyExistException(string message) :base(message) { }

    }
}
