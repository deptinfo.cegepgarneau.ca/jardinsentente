﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Regles
{
    public class RegleNotFoundException:Exception
    {
        public RegleNotFoundException(string message) : base(message) { }

    }
}
