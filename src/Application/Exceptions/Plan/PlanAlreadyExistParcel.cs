﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.Plan
{
    public class PlanAlreadyExistParcel:Exception
    {
        public PlanAlreadyExistParcel(string message):base(message) { }
    }
}
