﻿using System.Reflection;
using Application.Interfaces.Services.Books;
using Application.Interfaces.Services.Members;
using Application.Interfaces.Services.Notifications;
using Application.Interfaces.Services.Plan;
using Application.Interfaces.Services.Regles;
using Application.Interfaces.Services.Users;
using Application.Interfaces.Services.WaitingMembers;
using Application.Interfaces.Services.Events;
using Application.Services.Books;
using Application.Services.Members;
using Application.Services.Notifications;
using Application.Services.Plan;
using Application.Services.Regle;
using Application.Services.Users;
using Application.Services.WaitingMembers;
using Application.Services.Events;
using Application.Settings;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Slugify;
using Application.Services.NotificationsServices;
using Application.Services.Lots;

namespace Application;

public static class ConfigureServices
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());

        services.Configure<ApplicationSettings>(configuration.GetSection("Application"));

        services.AddScoped<ISlugHelper, SlugHelper>();

        services.AddScoped<IBookCreationService, BookCreationService>();
        services.AddScoped<IBookUpdateService, BookUpdateService>();
        services.AddScoped<INotificationService, EmailNotificationService>();
        services.AddScoped<IAuthenticatedUserService, AuthenticatedUserService>();
        services.AddScoped<IAuthenticatedMemberService, AuthenticatedMemberService>();
        services.AddScoped<IWaitingMemberCreationService, WaitingMemberCreationService>();
        services.AddScoped<IRegleUpdateService, RegleUopdateService>();
        services.AddScoped<IRegleCreationService, RegleCreationService>();
        services.AddScoped<IPlanCreationService, PlanCreationService>();
        services.AddScoped<IPlanUpdateService, PlanUpdateService>();
        services.AddScoped<IMemberCreationService, MemberCreationService>();
        services.AddScoped<IEventCreationService, EventCreationService>();
        services.AddScoped<Interfaces.Services.Lots.ILotUpdateService, LotUpdateService>();
        services.AddScoped<INotificationCreateService, NotificationCreateService>();
        services.AddScoped<IEventUpdateService, EventUpdateService>();
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        return services;
    }
}