﻿using Domain.Entities.Lots;

namespace Application.Interfaces.Services.Lots
{
    public interface ILotUpdateService
    {
        Task UpdateLot(int lotNumber, double newTemperature, double newHumidity);
    }
}
