﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Regles
{
    public interface IRegleCreationService
    {
        Task CreateRegle(Regle regle);
    }
}
