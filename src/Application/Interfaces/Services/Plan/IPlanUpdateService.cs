﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Plan
{
    public interface IPlanUpdateService
    {
        Task UpdatePLan(Domain.Entities.Plan.Plan plan);

    }
}
