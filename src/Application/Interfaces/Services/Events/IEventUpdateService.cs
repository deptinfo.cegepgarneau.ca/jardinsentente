﻿using Domain.Entities.Event;


namespace Application.Interfaces.Services.Events
{
    public interface IEventUpdateService
    {
        Task UpdateEvent(Event @event);
    }
}
