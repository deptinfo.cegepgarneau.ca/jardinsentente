﻿
using Domain.Entities.Event;



namespace Application.Interfaces.Services.Events
{
    public interface IEventCreationService
    {
        Task CreateEvent(Event @event);
    }
}
