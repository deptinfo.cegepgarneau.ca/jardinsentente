﻿using Domain.Entities;
using Domain.Entities.Lots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Members
{
    public interface IMemberCreationService
    {
        Task<Member> CreateMember(string firstName, string lastName, string email, string password, int? apartment, string? street, string? city, string? zipCode, string? phoneNumber, Lot lot);
    }
}
