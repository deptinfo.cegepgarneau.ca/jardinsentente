﻿using Domain.Entities.WaitingMembers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.WaitingMembers
{
    public interface IWaitingMemberCreationService
    {
        Task CreateWaitingMember(WaitingMember waitingMember);
    }
}
