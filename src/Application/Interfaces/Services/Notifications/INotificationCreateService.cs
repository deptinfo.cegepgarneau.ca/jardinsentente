﻿using Domain.Entities.Notifications;

namespace Application.Interfaces.Services.Notifications
{
    public interface INotificationCreateService
    {
        Task CreateNotification(Notification  notification);
    }
}
