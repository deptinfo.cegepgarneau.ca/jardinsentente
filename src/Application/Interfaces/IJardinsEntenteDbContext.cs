﻿using Domain.Entities;
using Domain.Entities.Books;
using Domain.Entities.Identity;
using Domain.Entities.WaitingMembers;
using Domain.Entities.Lots;
using Domain.Entities.Event;
using Microsoft.EntityFrameworkCore;
using Domain.Entities.Plan;
using Domain.Entities.Notifications;

namespace Application.Interfaces;

public interface IJardinsEntenteDbContext
{
    DbSet<Member> Members { get; }
    DbSet<Book> Books { get; }
    DbSet<Regle> Regles { get; }

    DbSet<Event> Events { get; }
    DbSet<Lot> Lots { get; }
    DbSet<Notification> Notifications { get; }

    DbSet<WaitingMember> WaitingMembers { get; }
    DbSet<UserLot> UserLots { get; }
    DbSet<Plan> Plans { get; }
    Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null);
}