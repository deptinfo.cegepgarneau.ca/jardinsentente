﻿using Domain.Entities;
using Domain.Entities.Lots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface ILotRepository
    {
        Lot FindByLotNumber(int lotNumber);
        List<Lot> GetLotsEmptyCurrentYear();
        Task UpdateLot(Lot lot);
    }
}
