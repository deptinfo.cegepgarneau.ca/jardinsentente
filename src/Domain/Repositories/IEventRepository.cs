﻿
using Domain.Entities.Books;
using Domain.Entities.Event;


namespace Domain.Repositories
{
    public interface IEventRepository
    {
        List<Event> GetAll();
        Event FindById(Guid id);
        Task CreateEvent(Event @event);
        Task UpdateEvent(Event @event);
        Task DeleteEventWithId(Guid id);

    }
}
