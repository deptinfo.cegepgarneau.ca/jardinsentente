﻿using Domain.Entities.Plan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IPlanRepository
    {
       // List<Plan> GetAll(Guid iduserlot,Guid idUserConnecte);
        List<Plan> GetAll(Guid iduserlot);
        Plan FindById(Guid id);
        Task CreatePlan(Plan plan, Guid userLotId);
        Task UpdatePLan(Plan plan);
        Task DeletePlanWithId(Guid id);
    }
}
