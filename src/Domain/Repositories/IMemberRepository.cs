﻿using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Entities.Lots;

namespace Domain.Repositories;

public interface IMemberRepository
{
    int GetMemberCount();
    List<Member> GetAll();
    List<Member> GetAllWithUserEmail(string userEmail);
    Member FindById(Guid id);
    Member? FindByUserId(Guid userId, bool asNoTracking = true);
    Member? FindByUserEmail(string userEmail);
    Task DeleteMemberWithId(Guid id);
    List<Member> GetAllAdmins();
    List<Member> GetMembersWithLotForCurrentYear();
    Task<Member> CreateMember(string firstName, string lastName, string email, string password, int? apartment, string? street, string? city, string? zipCode, string? phoneNumber, Lot lot);
   // List<Member> GetMembersWithLotForYear(int yearChoice);
    List<Member> GetAllMembersWithUser();

    Task AssignerLotDebutDanne(Guid idMember, int numeroLot);
}