﻿using Domain.Entities.Books;
using Domain.Entities.WaitingMembers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IWaitingMemberRepository
    {
        Task CreateWaitingMember(WaitingMember waitingMember);
        List<WaitingMember> GetAll();
        WaitingMember FindById(Guid id);
        bool FindByEmail(string email);
        Task DeleteWaitingMemberWithId(Guid id);
    }
}
