﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IUserLotRepository
    {
        Task AddUserLot(UserLot userLot);
        UserLot FindUserLotById(Guid userLotId);

        bool UserLotIsTheYear(Guid userLotId);

        List<UserLot> GetAllByIdUser(Guid idUser);

        List<UserLot> GetAllUserLotUtilisateurConnecte();

        void VerifierUserLotPourUser(UserLot userLot);

        Task DeleteUserLot(Guid id);
    }
}
