﻿using Domain.Entities;
using Domain.Entities.Books;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IRegleRepository
    {
        List<Regle> GetAll();
        Regle FindById(Guid id);
        Task CreateRegle(Regle regle);
        Task UpdateRegle(Regle regle);
        Task DeleteRegleWithId(Guid id);
    }
}
