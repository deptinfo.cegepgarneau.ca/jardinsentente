﻿using Domain.Entities.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface INotificationRepository
    {
        Task CreateNotification(Notification notification);
        
        List<Notification> GetNotificationsByMemberId(Guid? memberId);
    }
}
