﻿namespace Domain.Constants.User;

public static class Roles
{
    public const string ADMINISTRATOR = "admin";
    public const string USER = "user";
}