﻿using Domain.Common;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Event
{
    public class Event : AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string Month { get; set; } = default!;
        public string Jour { get; set; } = default!;
        public string DayOfWeek { get; set; } = default!;
        public string Description { get; set; } = default!;

        public string Slug { get; private set; } = default!;

        public Event(string month, string jour, string dayOfWeek, string description)
        {
            Month = month;
            Jour = jour;
            DayOfWeek = dayOfWeek;
            Description = description;
        }

        public void SetMonth(string month) => Month = month;
        public void SetJour(string jour) => Jour = jour;
        public void SetDayOfWeek(string dayOfWeek) => DayOfWeek = dayOfWeek;
        public void SetDescription(string description) => Description = description;
        public void SetSlug(string slug) => Slug = slug;

        public override void SoftDelete(string? deletedBy = null)
        {
            base.SoftDelete(deletedBy);
        }

        public void SanitizeForSaving()
        {
            Month = Month.Trim();
            Jour = Jour.Trim();
            DayOfWeek = DayOfWeek.Trim();
            Description = Description.Trim();
            Slug = Slug.Trim();
        }   


    }
}
