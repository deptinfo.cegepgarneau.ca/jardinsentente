﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Regle:AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string TitreFr { get; private set; } = default!;
        public string DescriptionFr { get; private set; } = default!;
        public string Slug { get; private set; } = default!;

        public void SetTitre(string titreFr)=>TitreFr = titreFr;
        public void SetDescription(string descriptionFr)=>DescriptionFr = descriptionFr;
        public void SetSlug(string slug)=>Slug = slug;

        public void SanitizeForSaving()
        {
           TitreFr=TitreFr.Trim();
            DescriptionFr=DescriptionFr.Trim();
            Slug=Slug.Trim();
        }

    }
}
