﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Common;

namespace Domain.Entities.Lots
{
    public class Lot : AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string Address { get; set; } = default!;
        public double Temperature { get; set; } = default!;
        public double Humidity { get; set; } = default!; 
        public double SunlightIntensity { get; set; } = default!;
        public int LotNumber { get; private set; } = default!;
        public List<UserLot> UserLots { get; set; } = default!;

        public Lot(string address, double temperature, double humidity, double sunlightIntensity, int lotNumber) 
        {
            Address = address;
            Temperature = temperature;
            Humidity = humidity;
            SunlightIntensity = sunlightIntensity;
            LotNumber = lotNumber;
        }

        

        public void SetAddress(string address) => Address = address;
        public void SetTemperature(double temperature) => Temperature = temperature;
        public void SetHumidity(double humidity) => Humidity = humidity;
        public void SetSunlightIntensity(double sunlightIntensity) => SunlightIntensity = sunlightIntensity;
        public void SetLotNumber(int lotNumber) => LotNumber = lotNumber;

        public override void SoftDelete(string? deletedBy = null)
        {
            base.SoftDelete(deletedBy);
        }

        public void SanitizeForSaving()
        {
            Address = Address.Trim();
            Temperature = Math.Round(Temperature, 2);
            Humidity = Math.Round(Humidity, 2);
            SunlightIntensity = Math.Round(SunlightIntensity, 2);
        }
    }
}
