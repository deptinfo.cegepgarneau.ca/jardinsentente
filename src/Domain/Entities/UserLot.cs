﻿using Domain.Common;
using Domain.Entities.Identity;
using Domain.Entities.Lots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class UserLot: AuditableAndSoftDeletableEntity, ISanitizable
    {
        public Lot Lot { get; private set; } = default!;
        public Guid LotId { get; private set; }=default!;
        public User User { get; private set; } = default!;
        public Guid UserId { get; private set; } = default!;
        public List<Plan.Plan> lstPlan { get; private set; } = default!;
        public int YearAssignment { get; private set; }
        public void SetLot(Lot lot) => Lot = lot;
        public void SetLotId(Guid lotId) => LotId = lotId;
        public bool IsActive() => Deleted == null && DeletedBy == null;

        public void SetUser(User user) => User = user;
        public void SetUserId(Guid userId) => UserId = userId;
        public void SetYearAssignment(int yearAssigment) => YearAssignment = yearAssigment;

        public void SanitizeForSaving()
        {
            //YearAssignment = YearAssignment;
        }
    }
}
