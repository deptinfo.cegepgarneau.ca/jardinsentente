﻿using Domain.Common;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Notifications
{
    public class Notification : AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string Title { get; private set; }
        public string Message { get; private set; }
        public Guid ToMemberId { get; private set; }
        public DateTime CreateAt { get; private set; }

        public void SetTitle(string title) => Title = title;
        public void SetMessage(string message) => Message = message;
        public void SetCreateAt(DateTime createAt) => CreateAt = createAt;
        public void SanitizeForSaving()
        {
            Title = Title.Trim();
            Message = Message.Trim();
        }
    }
}
