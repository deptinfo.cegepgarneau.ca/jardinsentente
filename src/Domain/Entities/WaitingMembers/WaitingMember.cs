﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.WaitingMembers
{
    public class WaitingMember : AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string FirstName { get; private set; } = default!;
        public string LastName { get; private set; } = default!;
        public string Adress {  get; private set; } = default!;
        public int? AppNumber { get; private set; }
        public string PostalCode { get; private set; } = default!;
        public string PhoneNumber { get; private set; } = default!;
        public string Email { get; private set; } = default!;
        public string From { get; private set; } = default!;

        public void SetFirstName(string firstName) => FirstName = firstName;
        public void SetLastName(string lastName) => LastName = lastName;
        public void SetAdress(string adress) => Adress = adress;
        public void SetAppNumber(int? appNumber) => AppNumber = appNumber;
        public void SetPostalCode(string postalCode) => PostalCode = postalCode;
        public void SetPhoneNumber(string phoneNumber) => PhoneNumber = phoneNumber;
        public void SetEmail(string email) => Email = email;
        public void SetFrom(string from) => From = from;
        
        public void SanitizeForSaving()
        {
            FirstName = FirstName.Trim();
            LastName = LastName.Trim();
            Adress = Adress.Trim();
            PostalCode = PostalCode.Trim();
            PhoneNumber = PhoneNumber.Trim();
            Email = Email.Trim();
            From = From.Trim();
        }
    }
}
