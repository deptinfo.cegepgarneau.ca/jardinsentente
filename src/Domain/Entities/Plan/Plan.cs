﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Plan
{
    public class Plan : AuditableAndSoftDeletableEntity, ISanitizable
    {
        public string Nom { get; private set; } = default!;
        public double X { get; private set; }=default!;
        public double Y { get; private set; }=default!;
        public double Largeur { get; private set; }=default!;
        public double Longueur { get; private set; }=default!;

        public Guid UserLotId { get; private set; } = default!;
        public UserLot UserLot { get; private set; } = default!;

        public void SetNom(string nom)=>Nom=nom;
        public void SetX(double x)=>X=x;
        public void SetY(double y)=>Y=y;
        public void SetLargeur(double largeur)=>Largeur=largeur;
        public void SetLongueur(double longueur)=>Longueur=longueur;
        public void SetUserLot(UserLot userlot)=>UserLot=userlot;

        public void SetIdUserLot(Guid userLotId)=>UserLotId=userLotId;
        public void SanitizeForSaving()
        {
            Nom = Nom.Trim();
        }
    }
}
