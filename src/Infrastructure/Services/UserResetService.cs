﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces.Services.Regles;
using Domain.Entities;
using Domain.Repositories;
using Google.Protobuf.WellKnownTypes;
using Infrastructure.Repositories.Users;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;

namespace Infrastructure.Services
{
    public class UserResetService : BackgroundService
    {
        private readonly ILogger<UserResetService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public UserResetService(ILogger<UserResetService> logger, IServiceScopeFactory serviceScopeFactory )
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            try
            {

                // Expression Cron pour le début de chaque année
                 string cronExpression = "0 0 1 1 *";
                
                // Expression Cron pour chaque minute
                //string cronExpression = "* * * * *";


                //string cronExpression = "0 13 * * 3";

                // Analyser l'expression Cron
                CrontabSchedule schedule = CrontabSchedule.Parse(cronExpression);

                
                // Attendre jusqu'au prochain début d'année
                DateTime nextOccurrence = schedule.GetNextOccurrence(DateTime.Now);
                 TimeSpan delay = nextOccurrence - DateTime.Now;
                while(delay.TotalMilliseconds> int.MaxValue)
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(int.MaxValue), stoppingToken);
                    delay = nextOccurrence - DateTime.Now;
                }
                await Task.Delay(delay, stoppingToken);


                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("Reintialisation des utilisateurs en cours");
                     await reintialiser();
                    _logger.LogInformation("Réintialisation des utilisateurs terminé avec succès");

                    // Attendre jusqu'au prochain début d'année
                    nextOccurrence = schedule.GetNextOccurrence(DateTime.Now);
                     delay = nextOccurrence - DateTime.Now;

                    while (delay.TotalMilliseconds > int.MaxValue)
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(int.MaxValue), stoppingToken);
                        delay = nextOccurrence - DateTime.Now;
                    }
                    await Task.Delay(delay, stoppingToken);

                }
            }
            catch (Exception ex)
            {

                _logger.LogError($"Une erreur s'est produite lors de la réintialisation : {ex.Message}");
            }



        }

        private async Task reintialiser()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                // Effectuer la réinitialisation des utilisateurs
                await userRepository.ReinstialiserAllUser();


            }
        }
    }
}
