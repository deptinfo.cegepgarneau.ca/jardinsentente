using Application.Exceptions.Members;
using Application.Interfaces;
using Domain.Constants.User;
using Domain.Entities;
using Domain.Entities.Books;
using Domain.Entities.Identity;
using Domain.Entities.Lots;
using Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure.Repositories.Members;

public class MemberRepository : IMemberRepository
{
    private readonly IJardinsEntenteDbContext _context;
    private readonly IUserRepository _userRepository;
    private readonly ILotRepository _lotRepository;
    private readonly IUserLotRepository _userLotRepository;
    private readonly UserManager<User> _userManager;


    public MemberRepository(UserManager<User> userManager,IJardinsEntenteDbContext context, IUserRepository userRepository,ILotRepository lotRepository, IUserLotRepository userLotRepository)
    {
        _userManager = userManager;
        _context = context;
        _userRepository = userRepository;
        _lotRepository = lotRepository;
        _userLotRepository = userLotRepository;
        _userManager = userManager;
    }

    public async Task<Member> CreateMember(string firstName, string lastName, string email, string password, int? apartment, string? street, string? city, string? zipCode, string? phoneNumber, Lot lot)
    {
        var existingUser = _context.Members.Any(m => m.User.Email != null && m.User.Email.ToLower().Trim() == email.ToLower().Trim());
        if (existingUser)
        {
            throw new MemberWithEmailAlreadyExistsException($"Un membre avec le courriel {email} existe d�j�.");
        }

        var newEmail = email.ToLower().Trim();
        var normalizedEmail = newEmail.Normalize();
        var newFirstName = firstName.Trim();
        var newLastName = lastName.Trim();
        var newPassword = password.Trim().Normalize();
        var newStreet = street != null ? street.Trim() : null;
        var newCity = city != null ? city.Trim() : null;
        var newPhoneNumber = phoneNumber != null ? phoneNumber.Trim() : null;

        User user = new()
        {
            Email = newEmail,
            UserName = newEmail,
            NormalizedEmail = normalizedEmail,
            NormalizedUserName = normalizedEmail,
            EmailConfirmed = true,
            TwoFactorEnabled = false,
            PhoneNumber = newPhoneNumber
        };

        IdentityResult? result = await _userManager.CreateAsync(user, newPassword);

        if(!result.Succeeded)
        {
            throw new Exception("Une erreur lors de la cr�ation de l'user.");
        }

        IdentityResult? resultRole = await _userManager.AddToRoleAsync(user, Roles.USER);
        if (!resultRole.Succeeded)
        {
            throw new Exception("Une erreur lors de l'attribution du r�le utilisateur durant la cr�ation du membre.");
        }


        Member? existingMember = _context.Members.FirstOrDefault(m => m.User.Id == user.Id);
        if(existingMember != null)
        {
            throw new Exception("Un membre existe pour cet User.");
        }

        Member newMember = new(newFirstName, newLastName, apartment, newStreet, newCity, zipCode);
        newMember.SetUser(user);
        newMember.Activate();
        _context.Members.Add(newMember);

        Lot? lotBd = _lotRepository.FindByLotNumber(lot.LotNumber);
        if (lotBd == null)
        {
            throw new Exception("Le lot n'a pas été trouvé dans la base de données.");
        }

        UserLot newUserLot = new();
        newUserLot.SetUserId(user.Id);
        newUserLot.SetLotId(lotBd.Id);
        newUserLot.SetYearAssignment(DateTime.Now.Year);
        await _userLotRepository.AddUserLot(newUserLot);

        await _context.SaveChangesAsync();

        return newMember;
    }

    public int GetMemberCount()
    {
        return _context.Members.Count();
    }

    public List<Member> GetAll()
    {
        return _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .AsNoTracking().ToList();
    }

    public List<Member> GetAllAdmins()
    {
        return _context.Members
            .Include(x => x.User)
                .ThenInclude(x => x.UserLots)
                    .ThenInclude(x => x.Lot)
            .Include(x => x.User)
                .ThenInclude(x => x.UserRoles)
                    .ThenInclude(x => x.Role)
            .Where(x => x.User.UserRoles.Any(r => r.Role.Name == "ADMIN"))
            .AsNoTracking().ToList();
    }

    public List<Member> GetMembersWithLotForCurrentYear()
    {
        int currentYear = DateTime.Now.Year;
        List<Member> ll = _context.Members
            .Include(x => x.User)
                .ThenInclude(x => x.UserLots)
                    .ThenInclude(x => x.Lot)
            .Include(x => x.User)
                .ThenInclude(x => x.UserLots)
            .Where(x =>x.User.UserLots.Any(l => l.YearAssignment == currentYear))
            .AsNoTracking().ToList();
        ll=ll.Where(x=>x.User.IsActive()).ToList();
        return ll;
    }

    public List<Member> GetAllWithUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .Where(x => x.User.Email == userEmail)
            .AsNoTracking()
            .ToList();
    }

    public Member FindById(Guid id)
    {
        var member = _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .Include(x => x.User)
                .ThenInclude(x => x.UserLots)
            .FirstOrDefault(x => x.Id == id);
        if (member == null)
            throw new MemberNotFoundException($"No member with id {id} was found.");
        return member;
    }

    public Member? FindByUserId(Guid userId, bool asNoTracking = true)
    {
        var query = _context.Members as IQueryable<Domain.Entities.Member>;
        if (asNoTracking)
            query = query.AsNoTracking();
        return query
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Id == userId);
    }

    public Member? FindByUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Email == userEmail);
    }

    public async Task DeleteMemberWithId(Guid id)
    {
        var member = _context.Members.Include(x => x.User).ThenInclude(x=>x.UserLots).FirstOrDefault(x => x.Id == id);
        if (member == null)
            throw new MemberNotFoundException($"Could not find member with id {id}.");
        int year=DateTime.Now.Year; 
       await _userRepository.DeleteUser(member.User);
        foreach (var userLot in member.User.UserLots)
        {
            if (userLot.YearAssignment == year)
                await _userLotRepository.DeleteUserLot(userLot.Id);
        }
    }
  
    public List<Member> GetAllMembersWithUser()
    {
        List<Member> lstMemberUser = _context.Members
    .Include(x => x.User)
    .ThenInclude(x => x.UserLots)
    .ThenInclude(x => x.Lot)
    .Include(X=>X.User)
    .ThenInclude(X=>X.UserRoles)
    .ThenInclude(X=>X.Role)
    .AsNoTracking().ToList();

        lstMemberUser= lstMemberUser.Where(X => X.User.RoleNames.Contains(Roles.USER) && !X.User.IsActive()).ToList();

        return lstMemberUser;
    }

    public async Task AssignerLotDebutDanne(Guid idMember, int numeroLot)
    {
        Member member = FindById(idMember);
        UserLot userLot = new ();
        userLot.SetUserId(member.User.Id);
        Lot lot= _lotRepository.FindByLotNumber(numeroLot);
        userLot.SetLotId(lot.Id);
        userLot.SetYearAssignment(DateTime.Now.Year);
        _userLotRepository.VerifierUserLotPourUser(userLot);
        await _userRepository.ActiverUser(member.User.Id);
        await _userLotRepository.AddUserLot(userLot);
    }


}