﻿using Application.Interfaces;
using Domain.Entities.Books;
using Domain.Entities.Lots;
using Domain.Repositories;
using Slugify;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Exceptions.Lot;
using Domain.Entities;

namespace Infrastructure.Repositories.Lots
{
    public class LotRepository : ILotRepository
    {
        private readonly ISlugHelper _slugHelper;
        private readonly IJardinsEntenteDbContext _context;
        public LotRepository(IJardinsEntenteDbContext context, ISlugHelper slugHelper)
        {
            _context = context;
            _slugHelper = slugHelper;
        }

        public Lot FindByLotNumber(int lotNumber)
        {
            var lot = _context.Lots
                .AsNoTracking()
                .FirstOrDefault(x => x.LotNumber == lotNumber);
            if (lot == null)
                throw new Exception($"Could not find lot with Lot Number {lotNumber}.");
            return lot;
        }

        public List<Lot> GetLotsEmptyCurrentYear()
        {
            int currentYear = DateTime.Now.Year;
            var listUserLot = _context.Lots.Include(l => l.UserLots).AsNoTracking().ToList();
            var listLotsEmptyThisYear = listUserLot.Where(l => !l.UserLots.Any(ul => ul.YearAssignment == currentYear && ul.IsActive()) );
            return listLotsEmptyThisYear.ToList();
        }

        public async Task UpdateLot(Lot lot)
        {
            _context.Lots.Update(lot);
            await _context.SaveChangesAsync();
        }
    }
}
