﻿using Application.Interfaces;
using Domain.Repositories;
using Slugify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stripe;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Repositories.UserLots;
using Domain.Entities;
using Application.Exceptions.Regles;
using Application.Exceptions.Plan;
using Domain.Entities.Plan;
using Xero.NetStandard.OAuth2.Model.Appstore;

namespace Infrastructure.Repositories.Plan
{
    public class PlanRepository : IPlanRepository
    {
        private readonly ISlugHelper _slugHelper;
        private readonly IJardinsEntenteDbContext _context;
        private readonly IUserLotRepository _userLotRepository;

        public PlanRepository(IJardinsEntenteDbContext context, ISlugHelper slugHelper,IUserLotRepository userLotRepository)
        {
            _context = context;
            _slugHelper = slugHelper;
            _userLotRepository = userLotRepository;
        }
        public async Task CreatePlan(Domain.Entities.Plan.Plan plan,Guid userLotId)
        {

            if (string.IsNullOrWhiteSpace(plan.Nom))
                  new ArgumentNullException("Veuillez remplir tous les informations de création d'un plan");
            UserLot userlot= _userLotRepository.FindUserLotById(userLotId);
            if (userlot is null)
                throw new ArgumentNullException();
            else if(userlot.YearAssignment<DateTime.Now.Year)
                throw new ArgumentException("Les plans d'anciens jardins ne peuvent pas être modifiés");

            plan.SetIdUserLot(userLotId);

            _context.Plans.Add(plan);
            await _context.SaveChangesAsync();
        }

        public async Task DeletePlanWithId(Guid id)
        {
            Domain.Entities.Plan.Plan plan = VerifierAnneePourValidite(id);

            _context.Plans.Remove(plan);
            await _context.SaveChangesAsync();
        }

        public Domain.Entities.Plan.Plan FindById(Guid id)
        {
            var plan = _context.Plans
                .AsNoTracking()
                 .FirstOrDefault(x => x.Id == id);
            if (plan == null)
                throw new PlanNotFoundException($" Ne trouve pas le plan avec l'ID {id}.");
            return plan;
        }



        public List<Domain.Entities.Plan.Plan> GetAll(Guid userlotId)
        {
            return _context.Plans.AsNoTracking().Where(x => x.UserLot.Id == userlotId).ToList();
        }

        public async Task UpdatePLan(Domain.Entities.Plan.Plan plan)
        {
            if (string.IsNullOrWhiteSpace(plan.Nom))
                throw new ArgumentNullException("Veuillez remplir tous les champs");

            Domain.Entities.Plan.Plan planTest = VerifierAnneePourValidite(plan.Id);

            List<Domain.Entities.Plan.Plan> planUser= _context.Plans.AsNoTracking().Where(x=>x.UserLot.Id==planTest.UserLotId).ToList();

            if (planUser.Any(x => ((plan.X<=x.X&&plan.Longueur>x.X-plan.X && plan.Y<=x.Y && plan.Largeur>x.Y-plan.Y) || (plan.X <= x.X && plan.Longueur > x.X - plan.X && plan.Y >= x.Y && x.Largeur > plan.Y - x.Y) ||
            (plan.X >= x.X && x.Longueur > plan.X - x.X && plan.Y <= x.Y && plan.Largeur > x.Y - plan.Y)|| (plan.X >= x.X && x.Longueur > plan.X - x.X && plan.Y >= x.Y && x.Largeur > plan.Y - x.Y))
            && x.Id!=plan.Id))
                throw new PlanAlreadyExistParcel("Un plan se situe déjà sur ce secteur");


            _context.Plans.Update(plan);
            await _context.SaveChangesAsync();
        }

        private void GenerateSlug(Domain.Entities.Plan.Plan plan)
        {
            
        }

        private Domain.Entities.Plan.Plan VerifierAnneePourValidite(Guid idPlan)
        {
            Domain.Entities.Plan.Plan? planTest = _context.Plans.Include(x => x.UserLot)
            .AsNoTracking()
                .FirstOrDefault(x => x.Id == idPlan);
            if (planTest is null)
                throw new PlanNotFoundException($"Ne trouve pas le plan à supprimer.");
            if (planTest.UserLot.YearAssignment < DateTime.Now.Year)
                throw new ArgumentException("Les plans d'anciens jardins ne peuvent pas être modifiés");
            return planTest;
        }
    }
}
