﻿using Application.Exceptions.Regles;
using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Regles
{
    public class RegleRepository : IRegleRepository
    {
        private readonly ISlugHelper _slugHelper;
        private readonly IJardinsEntenteDbContext _context;

        public RegleRepository(IJardinsEntenteDbContext context, ISlugHelper slugHelper)
        {
            _context = context;
            _slugHelper = slugHelper;
        }
        public async Task CreateRegle(Regle regle)
        {
            if (string.IsNullOrWhiteSpace(regle.TitreFr) || string.IsNullOrWhiteSpace(regle.DescriptionFr))
                throw new ArgumentNullException("Veuillez remplir tous les champs");
            if (_context.Regles.Any(x => x.TitreFr.Trim() == regle.TitreFr.Trim()))
                throw new RegleAlreadyExistException($"Une règle avec le titre {regle.TitreFr} existe déjà");
            GenerateSlug(regle);
            _context.Regles.Add(regle);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteRegleWithId(Guid id)
        {
            var regle = _context.Regles.FirstOrDefault(x => x.Id == id);
            if (regle == null)
                throw new RegleNotFoundException($"Ne trouve pas la règle avec l'ID{id}.");

            _context.Regles.Remove(regle);
            await _context.SaveChangesAsync();
        }

        public Regle FindById(Guid id)
        {
            var regle = _context.Regles
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == id);
            if (regle == null)
                throw new RegleNotFoundException($" Ne trouve pas la règle avec l'ID {id}.");
            return regle;
        }

        public List<Regle> GetAll()
        {
            return _context.Regles.AsNoTracking().ToList();

        }

        public async Task UpdateRegle(Regle regle)
        {
            if (string.IsNullOrWhiteSpace(regle.TitreFr) || string.IsNullOrWhiteSpace(regle.DescriptionFr))
                throw new ArgumentNullException("Veuillez remplir tous les champs");
            if (_context.Regles.Any(x => x.TitreFr.Trim() == regle.TitreFr.Trim() && x.Id!=regle.Id))
                throw new RegleAlreadyExistException($"Une règle avec le titre {regle.TitreFr} existe déjà");
            if (string.IsNullOrWhiteSpace(regle.Slug))
                GenerateSlug(regle);

            _context.Regles.Update(regle);
            await _context.SaveChangesAsync();
        }

        private void GenerateSlug(Regle regle)
        {
            var slug = regle.TitreFr;
            var slugs = _context.Books.AsNoTracking().Where(x => x.Slug == slug).ToList();
            if (slugs.Any())
                slug = $"{slug}-{slug.Length + 1}";
            regle.SetSlug(_slugHelper.GenerateSlug(slug).Replace(".", ""));
        }
    }
}
