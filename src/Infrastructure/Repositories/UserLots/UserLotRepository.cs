﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Exceptions.Regles;
using Application.Exceptions.UserLot;
using Application.Interfaces;
using Application.Interfaces.Services;
using Domain.Entities;
using Domain.Entities.Lots;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.UserLots
{
    public class UserLotRepository : IUserLotRepository
    {
        private readonly IJardinsEntenteDbContext _context;
        private readonly IHttpContextUserService _httpcontext;

        public UserLotRepository(IJardinsEntenteDbContext context, IHttpContextUserService httpConext)
        {
            _context = context;
            _httpcontext = httpConext;
        }

        public async Task AddUserLot(UserLot userLot)
        {
            VerifierUserLotPourUser(userLot);
            await UserLotExistsForLotCurrentYear(userLot);
            _context.UserLots.Add(userLot);
            await _context.SaveChangesAsync();
        }

        public async Task UserLotExistsForLotCurrentYear(UserLot userLot)
        {
            var userLotExists = await _context.UserLots.AnyAsync(x => x.Lot.Id == userLot.LotId && x.YearAssignment == userLot.YearAssignment);
            if (userLotExists)
                throw new UserLotAlreadyExist("Ce lot est déjà assignée pour l'année " + userLot.YearAssignment);
        }

        public List<UserLot> GetAllByIdUser(Guid idUser)
        {

            List<UserLot> ll = _context.UserLots.Include(x => x.User).Include(x => x.Lot).Where(x => x.User.Id == idUser).AsNoTracking().ToList();
            return ll;


        }

        public UserLot FindUserLotById(Guid userLotId)
        {
            var userLot = _context.UserLots
                .AsNoTracking()
                .FirstOrDefault(x => x.Id == userLotId);
            if (userLot == null)
                throw new UserLotNotFoundException($" Ne trouve pas l'user-lot avec l'ID {userLotId}.");
            return userLot;
        }

        public List<UserLot> GetAllUserLotUtilisateurConnecte()
        {
            var email = _httpcontext.UserEmail;
            return _context.UserLots.Include(x => x.Lot).Where(x => x.User.Email == email).AsNoTracking().ToList();
        }

        public bool UserLotIsTheYear(Guid userLotId)
        {
            var userLot = _context.UserLots
                .AsNoTracking()
                .FirstOrDefault(x => x.Id == userLotId && x.YearAssignment == DateTime.Now.Year);
            return userLot is not null;
        }

        public void VerifierUserLotPourUser(UserLot userLot)
        {
            if (_context.UserLots.Any(x => x.Lot.Id == userLot.LotId && x.User.Id == userLot.UserId && x.YearAssignment == userLot.YearAssignment))
                throw new UserLotAlreadyExist("Cet utilisateur dispose déjà d'un lot pour l'année " + userLot.YearAssignment);
        }

        public async Task DeleteUserLot(Guid id)
        {
            var userLot = _context.UserLots.FirstOrDefault(x => x.Id == id);
            if (userLot == null)
                throw new UserLotNotFoundException($"Ne trouve pas l'user-lot avec l'ID{id}.");

            _context.UserLots.Remove(userLot);
            await _context.SaveChangesAsync();
        }
    }
}
