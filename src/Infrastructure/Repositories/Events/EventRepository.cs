﻿using System;
using Application.Interfaces;
using Domain.Entities.Event;
using Domain.Repositories;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using Slugify;

namespace Infrastructure.Repositories.Events
{
    public class EventRepository : IEventRepository
    {
        private readonly ISlugHelper _slugHelper;
        private readonly IJardinsEntenteDbContext _context;

        public EventRepository(IJardinsEntenteDbContext context, ISlugHelper slugHelper)
        {
            _context = context;
            _slugHelper = slugHelper;
        }

        public List<Event> GetAll()
        {
            return _context.Events.AsNoTracking().ToList();
        }

        public Event FindById(Guid id)
        {
            var @event = _context.Events
                .AsNoTracking()
                .FirstOrDefault(x => x.Id == id);
            if (@event == null)
                throw new Exception($"Could not find event with id {id}.");
            return @event;
        }

        public async Task CreateEvent(Event @event)
        {
            if (_context.Events.Any(x => x.Month.Trim() == @event.Month.Trim() && x.Jour.Trim() == @event.Jour.Trim() && x.DayOfWeek.Trim() == @event.DayOfWeek.Trim() && x.Description.Trim() == @event.Description.Trim()))
                throw new Exception($"This event already exists.");
            GenerateSlug(@event);
            _context.Events.Add(@event);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateEvent(Event @event)
        {
            if (!_context.Events.Any(x => x.Id != @event.Id))
                throw new Exception($"A event does not exists with this id {@event.Id}.");

            if (string.IsNullOrWhiteSpace(@event.Slug))
                GenerateSlug(@event);

            _context.Events.Update(@event);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteEventWithId(Guid id)
        {
            var @event = _context.Events.FirstOrDefault(x => x.Id == id);
            if (@event == null)
                throw new Exception($"Could not find @event with id {id}.");

            _context.Events.Remove(@event);
            await _context.SaveChangesAsync();
        }

        private void GenerateSlug(Event @event)
        {
            var slug = @event.Description;
            var slugs = _context.Events.AsNoTracking().Where(x => x.Slug == slug).ToList();
            if (slugs.Any())
                slug = $"{slug}-{slug.Length + 1}";
            @event.SetSlug(_slugHelper.GenerateSlug(slug).Replace(".", ""));
        }
    }
}
