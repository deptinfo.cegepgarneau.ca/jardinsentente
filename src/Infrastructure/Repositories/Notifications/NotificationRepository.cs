﻿using Application.Interfaces;
using Domain.Entities.Lots;
using Domain.Entities.Notifications;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;

namespace Infrastructure.Repositories.Notifications
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly ISlugHelper _slugHelper;
        private readonly IJardinsEntenteDbContext _context;

        public NotificationRepository(IJardinsEntenteDbContext context, ISlugHelper slugHelper)
        {
            _context = context;
            _slugHelper = slugHelper;
        }

        public async Task CreateNotification(Notification notification)
        {
            if (string.IsNullOrWhiteSpace(notification.Title) || string.IsNullOrWhiteSpace(notification.Message) || notification.ToMemberId == null)
                throw new ArgumentNullException("Veuillez remplir tous les champs.");
            notification.SetCreateAt(DateTime.Now);
            _context.Notifications.Add(notification);
            await _context.SaveChangesAsync();
        }

        public List<Notification> GetNotificationsByMemberId(Guid? memberId)
        {
            return _context.Notifications
                .Where(x => x.ToMemberId == memberId)
                .OrderByDescending(x => x.CreateAt)
                .AsNoTracking()
                .ToList();
        }
    }
}
