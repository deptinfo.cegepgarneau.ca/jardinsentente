﻿using Application.Exceptions.Books;
using Application.Exceptions.Members;
using Application.Exceptions.Regles;
using Application.Exceptions.WaitingMembers;
using Application.Interfaces;
using AutoMapper.Execution;
using Domain.Entities.WaitingMembers;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.WaitingMembers
{

    public class WaitingMemberRepository : IWaitingMemberRepository
    {
        private readonly IJardinsEntenteDbContext _context;
        public WaitingMemberRepository(IJardinsEntenteDbContext context)
        {
            _context = context;
        }

        public async Task CreateWaitingMember(WaitingMember waitingMember)
        {
            _context.WaitingMembers.Add(waitingMember);
            await _context.SaveChangesAsync();
        }

        public WaitingMember FindById(Guid id)
        {
            var waitingMember = _context.WaitingMembers
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == id);
            if (waitingMember == null)
                throw new WaitingMemberNotFoundException($"Could not find waiting member with id {id}.");
            return waitingMember;
        }

        public List<WaitingMember> GetAll()
        {
            return _context.WaitingMembers.AsNoTracking().ToList();
        }

        public bool FindByEmail(string email) 
        {
            WaitingMember? waitingMember = _context.WaitingMembers.FirstOrDefault(x => x.Email == email);
            return (waitingMember != null);
        }

        public async Task DeleteWaitingMemberWithId(Guid id)
        {
            var waitingMember = _context.WaitingMembers.FirstOrDefault(x => x.Id == id);
            if (waitingMember == null)
                throw new WaitingMemberNotFoundException($"Could not find waiting member with id {id}.");
            _context.WaitingMembers.Remove(waitingMember);
            await _context.SaveChangesAsync();
        }
    }
}
